﻿using System.Collections.Generic;

namespace MiCons.Data.Entities
{
    public class Residencia : IEntity
    {
        public int Id { get; set; }

        public int IdConjunto { get; set; }

        public string Descripcion { get; set; }

        public string Telefono { get; set; }

        public Conjunto Conjunto { get; set; }

        public ICollection<Novedad> Novedades { get; set; }

        public ICollection<Residente> Residentes { get; set; }

        public Residencia()
        {
            Novedades = new HashSet<Novedad>();
            Residentes = new HashSet<Residente>();
        }
    }
}