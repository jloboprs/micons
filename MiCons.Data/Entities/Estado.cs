﻿using System.Collections.Generic;

namespace MiCons.Data.Entities
{
    public class Estado : IEntity
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public ICollection<Novedad> Novedades { get; set; }

        public Estado()
        {
            Novedades = new HashSet<Novedad>();
        }
    }
}