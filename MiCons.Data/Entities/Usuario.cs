﻿using System;

namespace MiCons.Data.Entities
{
    public class Usuario : IEntity
    {
        public int Id { get; set; }

        public int IdPersona { get; set; }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value != null ? value.ToLower() : null; }
        }

        public string Clave { get; set; }

        public Guid? CodigoActivacion { get; set; }

        public bool Activo { get; set; }

        public bool ReiniciarClave { get; set; }

        public Persona Persona { get; set; }
    }
}