﻿using System.Collections.Generic;
using System.Linq;
namespace MiCons.Data.Entities
{
    public class Persona : IEntity
    {
        public int Id { get; set; }

        public int IdTipoDocumento { get; set; }

        public string NumeroDocumento { get; set; }

        public string Nombre { get; set; }

        public string Iniciales
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Nombre))
                    return null;

                var nombres = Nombre.Split(' ').Where(palabra => palabra != string.Empty).ToList();
                var primerNombre = nombres[0];
                if (nombres.Count == 1)
                    return primerNombre.Substring(0, primerNombre.Length == 1 ? 1 : 2).ToUpper();

                var segundoNombre = nombres[1];
                return string.Format("{0}{1}", primerNombre[0], segundoNombre[0]).ToUpper();
            }
        }

        public string Foto { get; set; }

        public string Telefono { get; set; }

        public string Direccion { get; set; }

        public bool Activo { get; set; }

        public TipoDocumento TipoDocumento { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }

        public ICollection<Comentario> Comentarios { get; set; }

        public ICollection<Empleado> Empleados { get; set; }

        public ICollection<Novedad> NovedadesCreadas { get; set; }

        public ICollection<Novedad> NovedadesAsignadas { get; set; }

        public ICollection<Residente> Residentes { get; set; }

        public Persona()
        {
            Usuarios = new HashSet<Usuario>();
            Comentarios = new HashSet<Comentario>();
            Empleados = new HashSet<Empleado>();
            NovedadesCreadas = new HashSet<Novedad>();
            NovedadesAsignadas = new HashSet<Novedad>();
            Residentes = new HashSet<Residente>();
        }
    }
}