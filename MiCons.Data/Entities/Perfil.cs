﻿using System.Collections.Generic;

namespace MiCons.Data.Entities
{
    public class Perfil : IEntity
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public ICollection<Novedad> Novedades { get; set; }

        public ICollection<Empleado> Empleados { get; set; }

        public Perfil()
        {
            Novedades = new HashSet<Novedad>();
            Empleados = new HashSet<Empleado>();
        }
    }
}