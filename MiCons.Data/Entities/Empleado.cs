﻿namespace MiCons.Data.Entities
{
    public class Empleado : IEntity
    {
        public int Id { get; set; }

        public int IdPersona { get; set; }

        public int IdConjunto { get; set; }

        public int IdPerfil { get; set; }

        public Persona Persona { get; set; }

        public Conjunto Conjunto { get; set; }

        public Perfil Perfil { get; set; }
    }
}