﻿using System;

namespace MiCons.Data.Entities
{
    public class Comentario : IEntity
    {
        public int Id { get; set; }

        public int IdNovedad { get; set; }

        public int IdPersona { get; set; }

        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }

        public Novedad Novedad { get; set; }

        public Persona Persona { get; set; }
    }
}