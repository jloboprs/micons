﻿namespace MiCons.Data.Entities
{
    public class Residente : IEntity
    {
        public int Id { get; set; }

        public int IdPersona { get; set; }

        public int IdResidencia { get; set; }

        public Persona Persona { get; set; }

        public Residencia Residencia { get; set; }
    }
}