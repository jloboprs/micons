﻿using System.ComponentModel.DataAnnotations;

namespace MiCons.Data.Entities
{
    public interface IEntity
    {
        [Key]
        int Id { get; set; }
    }
}