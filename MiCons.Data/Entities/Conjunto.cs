﻿using System.Collections.Generic;

namespace MiCons.Data.Entities
{
    public class Conjunto : IEntity
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public ICollection<Empleado> Empleados { get; set; }

        public ICollection<Residencia> Residencias { get; set; }

        public ICollection<Novedad> Novedades { get; set; }

        public Conjunto()
        {
            Empleados = new HashSet<Empleado>();
            Residencias = new HashSet<Residencia>();
            Novedades = new HashSet<Novedad>();
        }
    }
}