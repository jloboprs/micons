﻿using System;
using System.Collections.Generic;

namespace MiCons.Data.Entities
{
    public class Novedad : IEntity
    {
        public int Id { get; set; }

        public int IdResidencia { get; set; }

        public int IdConjunto { get; set; }

        public int IdPersonaCreada { get; set; }

        public int? IdPersonaAsignada { get; set; }

        public int? IdPerfilAsignado { get; set; }

        public int IdEstado { get; set; }

        public string Asunto { get; set; }

        public string Descripcion { get; set; }

        public byte[] VersionFila { get; set; }

        public DateTime Fecha { get; set; }

        public Residencia Residencia { get; set; }

        public Conjunto Conjunto { get; set; }

        public Persona PersonaCreada { get; set; }

        public Persona PersonaAsignada { get; set; }

        public Perfil PerfilAsignado { get; set; }

        public Estado Estado { get; set; }

        public ICollection<Comentario> Comentarios { get; set; }

        public Novedad()
        {
            Comentarios = new HashSet<Comentario>();
        }
    }
}