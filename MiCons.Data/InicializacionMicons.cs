﻿using MiCons.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;

namespace MiCons.Data
{
    public class InicializacionMicons : DropCreateDatabaseIfModelChanges<MiConsDb>
    {
        protected override void Seed(MiConsDb context)
        {
            var administrador = new Perfil { Descripcion = "Administrador" };
            var perfiles = new List<Perfil>
            {
                administrador,
                new Perfil {Descripcion = "Contador" },
                new Perfil {Descripcion = "Limpieza" },
            };

            var cedula = new TipoDocumento { Codigo = "C.C.", Descripcion = "Cédula de ciudadanía" };
            var tipoDocumentos = new List<TipoDocumento>
            {
                cedula,
                new TipoDocumento { Codigo = "C.E.", Descripcion = "Cédula de extranjería" },
                new TipoDocumento { Codigo = "NIT", Descripcion = "NIT" },
                new TipoDocumento { Codigo = "PA", Descripcion = "Pasaporte" },
                new TipoDocumento { Codigo = "R.C.", Descripcion = "Registro Civil" },
                new TipoDocumento { Codigo = "T.E.", Descripcion = "Tarjeta de extranjería" },
                new TipoDocumento { Codigo = "T.I.", Descripcion = "Tarjeta de identidad" }
            };

            var estados = new List<Estado>
            {
                new Estado{ Descripcion = "Abierto" },
                new Estado{ Descripcion = "EnCurso" },
                new Estado{ Descripcion = "cerrado" }
            };

            var conjunto = new Conjunto
            {
                Nombre = "Los Cocos",
                Direccion = "Calle 130 # 45A - 12",
                Telefono = "3254789",
                Residencias = new List<Residencia>
                {
                    new Residencia { Descripcion = "101" },
                    new Residencia { Descripcion = "102" },
                    new Residencia { Descripcion = "103" }
                }
            };

            var persona = new Persona
            {
                TipoDocumento = cedula,
                NumeroDocumento = "11223344",
                Nombre = "Admin",
                Foto = "aaaa",
                Telefono = "3145458825",
                Direccion = "Calle 85 # 16 - 13",
                Activo = true,
                Usuarios = new List<Usuario>
                {
                    new  Usuario
                    {
                        Email = "admin@micons.com",
                        Clave = "6crja3sAI3WxoQeHEUZ16ITmNYQM4khosEUuShFYugDIUrYbL96OdmB7sJiFVbzuF3roF27oIlUaZAx0E9o1D5E/rjSZ",
                        Activo = true,
                        ReiniciarClave = false
                    }
                }
            };

            var empleado = new Empleado
            {
                Persona = persona,
                Conjunto = conjunto,
                Perfil = administrador
            };

            context.Perfiles.AddRange(perfiles);
            context.TipoDocumentos.AddRange(tipoDocumentos);
            context.Estados.AddRange(estados);
            context.Empleados.Add(empleado);

            context.SaveChanges();
        }
    }
}