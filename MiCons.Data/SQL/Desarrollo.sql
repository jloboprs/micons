insert into dbo.Conjuntos (Nombre, Direccion, Telefono) values ('Los Cocos', 'Calle 130 # 45A - 12', '3254789');
insert into dbo.Conjuntos (Nombre, Direccion, Telefono) values ('Altos de castilla', 'Calle 130 # 45A - 12', '3254789');
insert into dbo.Conjuntos (Nombre, Direccion, Telefono) values ('El Porvenir', 'Calle 130 # 45A - 12', '3254789');

insert into dbo.Residencias(IdConjunto, Descripcion) values(1, '101') --Los Cocos
insert into dbo.Residencias(IdConjunto, Descripcion) values(1, '102') --Los Cocos
insert into dbo.Residencias(IdConjunto, Descripcion) values(1, '103') --Los Cocos

insert into dbo.Residencias(IdConjunto, Descripcion) values(2, '101') --Altos de castilla
insert into dbo.Residencias(IdConjunto, Descripcion) values(2, '102') --Altos de castilla
insert into dbo.Residencias(IdConjunto, Descripcion) values(2, '103') --Altos de castilla

insert into dbo.Residencias(IdConjunto, Descripcion) values(3, '101') --El Porvenir
insert into dbo.Residencias(IdConjunto, Descripcion) values(3, '102') --El Porvenir
insert into dbo.Residencias(IdConjunto, Descripcion) values(3, '103') --El Porvenir

insert into dbo.Personas (IdTipoDocumento, NumeroDocumento, Nombre, Telefono, Direccion, Activo) values (1, '11223344', 'Admin', '3145458825', 'Calle 85 # 16 - 13', 1);
insert into dbo.Personas (IdTipoDocumento, NumeroDocumento, Nombre, Telefono, Direccion, Activo) values (1, '984545574', 'Nathaly Pinzon', '3017267485', 'Calle 85 # 16 - 13', 1);
insert into dbo.Personas (IdTipoDocumento, NumeroDocumento, Nombre, Telefono, Direccion, Activo) values (1, '1154874582', 'Carlos Camargo', '3025478941', 'Calle 85 # 16 - 13', 1);

insert into dbo.Usuarios(IdPersona, Email, Clave, Activo, ReiniciarClave) values (1, 'admin@micons.com', '6crja3sAI3WxoQeHEUZ16ITmNYQM4khosEUuShFYugDIUrYbL96OdmB7sJiFVbzuF3roF27oIlUaZAx0E9o1D5E/rjSZ', 1, 0);
insert into dbo.Usuarios(IdPersona, Email, Clave, Activo, ReiniciarClave) values (2, 'npinzon@micons.com', '96BgA9Z+TON/qlVETBFxJt50ksbiGTC2QpWUlnOEtmZ791zjbFzohobWzOqAZZSccuA3qp0wSKcq9tZWCZydRlf+XIKB', 1, 0);
insert into dbo.Usuarios(IdPersona, Email, Clave, Activo, ReiniciarClave) values (3, 'ccamargo@micons.com', '2TCakbWyoigBajf9RgetfM2IFgmxPsFJc0LN/HZwvJlAdYl6mRxZ03wIWw3F3MqR16Ex74H0aAeWta7KTwLXRsEUb6dr', 1, 0);

insert into dbo.Empleados (IdPerfil, IdPersona, IdConjunto) values (1, 1, 1) --Jose Luis
insert into dbo.Empleados (IdPerfil, IdPersona, IdConjunto) values (1, 1, 2) --Jose Luis
insert into dbo.Empleados (IdPerfil, IdPersona, IdConjunto) values (1, 1, 3) --Jose Luis
insert into dbo.Empleados (IdPerfil, IdPersona, IdConjunto) values (2, 2, 1) --Nathaly Pinzon
insert into dbo.Residentes (IdPersona, IdResidencia) values (3, 1) --Carlos Camargo

insert into dbo.Novedades (IdResidencia, IdConjunto, IdPersonaCreada, IdPersonaAsignada, IdPerfilAsignado, IdEstado, Asunto, Descripcion, Fecha) values(1, 1, 3, null, null, 1, 'Se rompio el tubo', 'el tubo esta roto', getdate());
insert into dbo.Novedades (IdResidencia, IdConjunto, IdPersonaCreada, IdPersonaAsignada, IdPerfilAsignado, IdEstado, Asunto, Descripcion, Fecha) values(1, 1, 3, 2, null, 1, 'Se fuindio la luz', 'en el apartamento 405', getdate());