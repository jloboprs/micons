insert into dbo.Perfiles(Descripcion) values('Administrador');
insert into dbo.Perfiles(Descripcion) values('Contador');

insert into dbo.TipoDocumentos(Descripcion, Codigo) values('C�dula de ciudadan�a', 'C.C.');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('C�dula de extranjer�a', 'C.E.');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('NIT', 'NIT');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('Pasaporte', 'PA');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('Registro Civil', 'R.C.');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('Tarjeta de extranjer�a', 'T.E.');
insert into dbo.TipoDocumentos(Descripcion, Codigo) values('Tarjeta de identidad', 'T.I.');

insert into dbo.Estados(Descripcion) values('Abierto');
insert into dbo.Estados(Descripcion) values('EnCurso');
insert into dbo.Estados(Descripcion) values('cerrado');