﻿using System.Collections.Generic;

namespace MiCons.Data.Models
{
    public class ModeloConsulta
    {
        public int Take { get; set; }
        public int Page { get; set; }
        public bool IsPageable { get; set; }
        public string OrderBy { get; set; }
        public bool Ascending { get; set; }
        public IEnumerable<ModeloFiltro> Filters { get; set; }
        public int Skip { get { return (Page > 1 ? Page - 1 : 0) * Take; } }

        public ModeloConsulta()
        {
            Ascending = true;
            IsPageable = true;
        }
    }
}
