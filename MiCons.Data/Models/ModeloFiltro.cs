﻿namespace MiCons.Data.Models
{
    public class ModeloFiltro
    {
        public string Field { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }
}
