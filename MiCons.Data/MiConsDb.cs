using MiCons.Data.Entities;
using MiCons.Data.Entities.Configurations;
using System.Data.Entity;
using System;

namespace MiCons.Data
{
    public class MiConsDb : DbContext, IMiConsDb
    {
        public MiConsDb() : base("name=MiConsDb") { }

        public virtual DbSet<Comentario> Comentarios { get; set; }
        public virtual DbSet<Conjunto> Conjuntos { get; set; }
        public virtual DbSet<Empleado> Empleados { get; set; }
        public virtual DbSet<Estado> Estados { get; set; }
        public virtual DbSet<Novedad> Novedades { get; set; }
        public virtual DbSet<Perfil> Perfiles { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<Residencia> Residencias { get; set; }
        public virtual DbSet<Residente> Residentes { get; set; }
        public virtual DbSet<TipoDocumento> TipoDocumentos { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ComentarioConfiguration());
            modelBuilder.Configurations.Add(new ConjuntoConfiguration());
            modelBuilder.Configurations.Add(new EmpleadoConfiguration());
            modelBuilder.Configurations.Add(new EstadoConfiguration());
            modelBuilder.Configurations.Add(new NovedadConfiguration());
            modelBuilder.Configurations.Add(new PerfilConfiguration());
            modelBuilder.Configurations.Add(new PersonaConfiguration());
            modelBuilder.Configurations.Add(new ResidenciaConfiguration());
            modelBuilder.Configurations.Add(new ResidenteConfiguration());
            modelBuilder.Configurations.Add(new TipoDocumentoConfiguration());
            modelBuilder.Configurations.Add(new UsuarioConfiguration());
        }
    }
}