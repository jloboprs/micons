﻿using MiCons.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MiCons.Data
{
    public static class Extensions
    {
        public static IQueryable<T> Where<T>(this IQueryable<T> data, IEnumerable<ModeloFiltro> filters)
        {
            var typeEntity = Expression.Parameter(typeof(T), "entity");
            Expression body = null;

            foreach (var filter in filters)
            {
                var left = Expression.Property(typeEntity, filter.Field);
                var right = Expression.Constant(Cast(filter.Value, left.Type), left.Type);
                var equal = GetComparator(filter, left, right);

                body = body == null ? equal : Expression.And(body, equal);
            }

            return data.Where(Expression.Lambda<Func<T, bool>>(body, typeEntity));
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> data, Expression<Func<T, object>> property, bool ascending = true)
        {
            var unary = property.Body as UnaryExpression;
            var body = unary != null ? unary.Operand : property.Body;
            var parameter = property.Parameters[0];

            return InvokeOrderBy(data, GetOrderMethodName(ascending), body, parameter);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> data, string property, bool ascending = true)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "entity");
            Expression expr = arg;

            foreach (string prop in props)
            {
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }

            return InvokeOrderBy(data, GetOrderMethodName(ascending), expr, arg);
        }

        private static string GetOrderMethodName(bool ascending = true)
        {
            return ascending ? "OrderBy" : "OrderByDescending";
        }

        private static IOrderedQueryable<T> InvokeOrderBy<T>(IQueryable<T> data, string methodName, Expression body, ParameterExpression parameter)
        {
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), body.Type);
            LambdaExpression lambda = Expression.Lambda(delegateType, body, parameter);

            var result = typeof(Queryable).GetMethods()
                .Single(method => method.Name == methodName
                    && method.IsGenericMethodDefinition
                    && method.GetGenericArguments().Length == 2
                    && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), body.Type)
                .Invoke(null, new object[] { data, lambda });

            return (IOrderedQueryable<T>)result;
        }

        private static Expression GetComparator(ModeloFiltro filter, MemberExpression left, ConstantExpression right)
        {
            switch (filter.Operator)
            {
                case "eq": return Expression.Equal(left, right);
                case "neq": return Expression.NotEqual(left, right);
                case "lt": return Expression.LessThan(left, right);
                case "lte": return Expression.LessThanOrEqual(left, right);
                case "gt": return Expression.GreaterThan(left, right);
                case "gte": return Expression.GreaterThanOrEqual(left, right);
                case "startswith": return Expression.Call(left, left.Type.GetMethod("StartsWith", new[] { typeof(string) }), right);
                case "endswith": return Expression.Call(left, left.Type.GetMethod("EndsWith", new[] { typeof(string) }), right);
                case "contains": return Expression.Call(left, left.Type.GetMethod("Contains"), right);
                case "doesnotcontain": return Expression.Not(Expression.Call(left, left.Type.GetMethod("Contains"), right));
            }

            return Expression.Equal(left, right);
        }

        private static object Cast(object data, Type type)
        {
            if (type == typeof(string))
                return data;

            if (type == typeof(int))
                return Convert.ToInt32(data);

            if (type == typeof(int?))
                return Convert.ToInt32(data) as int?;

            if (type == typeof(decimal))
                return Convert.ToDecimal(data);

            if (type == typeof(decimal?))
                return Convert.ToDecimal(data) as decimal?;

            if (type == typeof(DateTime))
                return Convert.ToDateTime(data);

            if (type == typeof(DateTime?))
                return Convert.ToDateTime(data) as DateTime?;

            if (type == typeof(bool))
                return Convert.ToBoolean(data);

            if (type == typeof(bool?))
                return Convert.ToBoolean(data) as bool?;

            if (type == typeof(float))
                return Convert.ToSingle(data);

            if (type == typeof(float?))
                return Convert.ToSingle(data) as float?;

            if (type == typeof(double))
                return Convert.ToDouble(data);

            if (type == typeof(double?))
                return Convert.ToDouble(data) as double?;

            return data;
        }
    }
}