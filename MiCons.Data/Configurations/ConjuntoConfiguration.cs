﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class ConjuntoConfiguration : EntityTypeConfiguration<Conjunto>
    {
        public ConjuntoConfiguration()
        {
            ToTable("Conjuntos");

            Property(e => e.Nombre)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.Direccion)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.Telefono)
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}