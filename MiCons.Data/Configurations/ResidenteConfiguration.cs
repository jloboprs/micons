﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class ResidenteConfiguration : EntityTypeConfiguration<Residente>
    {
        public ResidenteConfiguration()
        {
            ToTable("Residentes");

            HasRequired(e => e.Persona)
                .WithMany(e => e.Residentes)
                .HasForeignKey(e => e.IdPersona)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Residencia)
                .WithMany(e => e.Residentes)
                .HasForeignKey(e => e.IdResidencia)
                .WillCascadeOnDelete(false);
        }
    }
}