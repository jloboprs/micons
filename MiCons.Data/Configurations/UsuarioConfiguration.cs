﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class UsuarioConfiguration : EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfiguration()
        {
            ToTable("Usuarios");

            HasRequired(e => e.Persona)
                .WithMany(e => e.Usuarios)
                .HasForeignKey(e => e.IdPersona)
                .WillCascadeOnDelete(true);

            Property(e => e.Email)
                .HasMaxLength(90)
                .IsRequired()
                .IsUnicode(true)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_Usuarios_Email") { IsUnique = true }));

            Property(e => e.Clave)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);
        }
    }
}