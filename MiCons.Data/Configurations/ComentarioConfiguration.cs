﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class ComentarioConfiguration : EntityTypeConfiguration<Comentario>
    {
        public ComentarioConfiguration()
        {
            ToTable("Comentarios");

            HasRequired(e => e.Novedad)
                .WithMany(e => e.Comentarios)
                .HasForeignKey(e => e.IdNovedad)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Persona)
                .WithMany(e => e.Comentarios)
                .HasForeignKey(e => e.IdPersona)
                .WillCascadeOnDelete(false);

            Property(e => e.Descripcion)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);
        }
    }
}