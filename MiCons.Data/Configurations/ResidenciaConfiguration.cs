﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class ResidenciaConfiguration : EntityTypeConfiguration<Residencia>
    {
        public ResidenciaConfiguration()
        {
            ToTable("Residencias");

            HasRequired(e => e.Conjunto)
                .WithMany(e => e.Residencias)
                .HasForeignKey(e => e.IdConjunto)
                .WillCascadeOnDelete(false);

            Property(e => e.Descripcion)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.Telefono)
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}