﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class EstadoConfiguration : EntityTypeConfiguration<Estado>
    {
        public EstadoConfiguration()
        {
            ToTable("Estados");

            Property(e => e.Descripcion)
                .HasMaxLength(50)
                .IsRequired()
                .IsUnicode(true);
        }
    }
}