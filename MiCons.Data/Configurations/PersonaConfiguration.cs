﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class PersonaConfiguration : EntityTypeConfiguration<Persona>
    {
        public PersonaConfiguration()
        {
            ToTable("Personas");

            HasRequired(e => e.TipoDocumento)
                .WithMany(e => e.Personas)
                .HasForeignKey(e => e.IdTipoDocumento)
                .WillCascadeOnDelete(false);

            Ignore(e => e.Iniciales);

            Property(e => e.IdTipoDocumento)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_Usuarios_Tipo_Numero_Documento", 1) { IsUnique = true }));

            Property(e => e.NumeroDocumento)
                .HasMaxLength(50)
                .IsRequired()
                .IsUnicode(false)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_Usuarios_Tipo_Numero_Documento", 2) { IsUnique = true }));

            Property(e => e.Nombre)
                .HasMaxLength(255)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.Foto)
                .HasMaxLength(255)
                .IsUnicode(false);

            Property(e => e.Telefono)
                .HasMaxLength(50)
                .IsUnicode(false);

            Property(e => e.Direccion)
                .HasMaxLength(255)
                .IsUnicode(true);
        }
    }
}