﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class TipoDocumentoConfiguration : EntityTypeConfiguration<TipoDocumento>
    {
        public TipoDocumentoConfiguration()
        {
            ToTable("TipoDocumentos");

            Property(e => e.Descripcion)
                .HasMaxLength(10)
                .IsRequired()
                .IsUnicode(false);

            Property(e => e.Descripcion)
                .HasMaxLength(50)
                .IsRequired()
                .IsUnicode(true);
        }
    }
}