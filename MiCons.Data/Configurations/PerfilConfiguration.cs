﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class PerfilConfiguration : EntityTypeConfiguration<Perfil>
    {
        public PerfilConfiguration()
        {
            ToTable("Perfiles");

            Property(e => e.Descripcion)
                .HasMaxLength(50)
                .IsRequired()
                .IsUnicode(true);
        }
    }
}