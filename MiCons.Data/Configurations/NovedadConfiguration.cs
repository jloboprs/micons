﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class NovedadConfiguration : EntityTypeConfiguration<Novedad>
    {
        public NovedadConfiguration()
        {
            ToTable("Novedades");

            HasRequired(e => e.Residencia)
                .WithMany(e => e.Novedades)
                .HasForeignKey(e => e.IdResidencia)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Conjunto)
                .WithMany(e => e.Novedades)
                .HasForeignKey(e => e.IdConjunto)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.PersonaCreada)
                .WithMany(e => e.NovedadesCreadas)
                .HasForeignKey(e => e.IdPersonaCreada)
                .WillCascadeOnDelete(false);

            HasOptional(e => e.PersonaAsignada)
                .WithMany(e => e.NovedadesAsignadas)
                .HasForeignKey(e => e.IdPersonaAsignada)
                .WillCascadeOnDelete(false);

            HasOptional(e => e.PerfilAsignado)
                .WithMany(e => e.Novedades)
                .HasForeignKey(e => e.IdPerfilAsignado)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Estado)
                .WithMany(e => e.Novedades)
                .HasForeignKey(e => e.IdEstado)
                .WillCascadeOnDelete(false);

            Property(e => e.Asunto)
                .HasMaxLength(50)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.Descripcion)
                .HasMaxLength(null)
                .IsRequired()
                .IsUnicode(true);

            Property(e => e.VersionFila)
                .IsConcurrencyToken()
                .IsRequired();
        }
    }
}