﻿using System.Data.Entity.ModelConfiguration;

namespace MiCons.Data.Entities.Configurations
{
    public class EmpleadoConfiguration : EntityTypeConfiguration<Empleado>
    {
        public EmpleadoConfiguration()
        {
            ToTable("Empleados");

            HasRequired(e => e.Persona)
                .WithMany(e => e.Empleados)
                .HasForeignKey(e => e.IdPersona)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Conjunto)
                .WithMany(e => e.Empleados)
                .HasForeignKey(e => e.IdConjunto)
                .WillCascadeOnDelete(false);

            HasRequired(e => e.Perfil)
                .WithMany(e => e.Empleados)
                .HasForeignKey(e => e.IdPerfil)
                .WillCascadeOnDelete(false);

        }
    }
}