﻿using MiCons.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class PersonaRepo : RepoBase<Persona>, IPersonaRepo
    {
        public PersonaRepo(IMiConsDb db) : base(db)
        {
        }

        public Persona ObtenerPorDocumento(int idTipoDocumento, string numeroDocumento)
        {
            return Db.Personas.AsNoTracking().SingleOrDefault(persona => persona.IdTipoDocumento == idTipoDocumento && persona.NumeroDocumento == numeroDocumento);
        }

        public Persona ObtenerPorEmail(string email)
        {
            return (from usuario in Db.Usuarios
                    join persona in Db.Personas on usuario.IdPersona equals persona.Id
                    where usuario.Email == email
                    select persona).AsNoTracking().SingleOrDefault();
        }

        public bool EsResidente(int idPersona)
        {
            return Db.Residentes.Any(residente => residente.IdPersona == idPersona);
        }

        public bool EsEmpleado(int idPersona)
        {
            return Db.Empleados.Any(empleado => empleado.IdPersona == idPersona);
        }

        public bool EsAdministrador(int idPersona)
        {
            return Db.Empleados.Any(empleado => empleado.IdPersona == idPersona && empleado.IdPerfil == (int)Perfiles.Administrador);
        }

        public bool ExistePersona(int idTipoDocumento, string numeroDocumento)
        {
            return Db.Personas.Any(persona => persona.IdTipoDocumento == idTipoDocumento && persona.NumeroDocumento == numeroDocumento && persona.Activo);
        }

        public IEnumerable<Persona> ObtenerEmpleados()
        {
            return (from persona in Db.Personas.AsNoTracking()
                    where Db.Empleados.Select(empleado => empleado.IdPersona).Contains(persona.Id)
                    select persona).ToList();
        }

        public Persona ObtenerPorUsuarioId(int idUsuario)
        {
            return Db.Usuarios.AsNoTracking().Where(usuario => usuario.Id == idUsuario).Select(usuario => usuario.Persona).SingleOrDefault();
        }

        public Persona Activar(Persona persona)
        {
            var personaDb = ObtenerPorId(persona.Id);
            if (personaDb == null)
                return null;

            personaDb.Activo = persona.Activo;
            Db.SaveChanges();

            return personaDb;
        }
    }
}