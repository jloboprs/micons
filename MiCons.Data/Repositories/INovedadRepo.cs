﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System.Collections.Generic;

namespace MiCons.Data.Repositories
{
    public interface INovedadRepo : IRepoBase<Novedad>
    {
        IEnumerable<Novedad> ObtenerPorAdministrador(int idPersona, ModeloConsulta request);
        IEnumerable<Novedad> ObtenerPorEmpleado(int idPersona, ModeloConsulta request);
        IEnumerable<Novedad> ObtenerPorResidente(int idPersona, ModeloConsulta request);
    }
}