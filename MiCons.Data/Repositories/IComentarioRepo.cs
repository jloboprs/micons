﻿using System.Collections.Generic;
using MiCons.Data.Entities;

namespace MiCons.Data.Repositories
{
    public interface IComentarioRepo : IRepoBase<Comentario>
    {
        IEnumerable<Comentario> ObtenerPorNovedad(int idNovedad);
    }
}