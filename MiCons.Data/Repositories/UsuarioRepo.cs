﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class UsuarioRepo : RepoBase<Usuario>, IUsuarioRepo
    {
        public UsuarioRepo(IMiConsDb db) : base(db) { }

        public Usuario ObtenerPorEmail(string email)
        {
            return Db.Usuarios.AsNoTracking().SingleOrDefault(usuario => usuario.Email == email);
        }

        public Usuario ObtenerPorEmailConPersona(string email)
        {
            return Db.Usuarios.AsNoTracking().Include(usuario => usuario.Persona).SingleOrDefault(usuario => usuario.Email == email);
        }

        public IEnumerable<Usuario> ObtenerConPersona(ModeloConsulta solicitud)
        {
            Set = Db.Usuarios.AsNoTracking().Include(usuario => usuario.Persona);

            return base.Obtener(solicitud);
        }

        public override Usuario Agregar(Usuario usuario)
        {
            MarcarCambio(usuario.Persona);

            usuario.CodigoActivacion = Guid.NewGuid();
            usuario.Activo = false;

            return base.Agregar(usuario);
        }

        public override Usuario Modificar(Usuario usuario)
        {
            MarcarCambio(usuario.Persona);

            var usuarioModificado = ObtenerPorId(usuario.Id);
            usuarioModificado.IdPersona = usuario.IdPersona;
            usuarioModificado.Email = usuario.Email;
            usuarioModificado.Activo = usuario.Activo;
            usuarioModificado.ReiniciarClave = usuario.ReiniciarClave;

            if (!string.IsNullOrWhiteSpace(usuario.Clave))
                usuarioModificado.Clave = usuario.Clave;

            if (!usuario.Activo)
                usuarioModificado.CodigoActivacion = usuario.CodigoActivacion = Guid.NewGuid();

            if (usuario.Persona != null)
                usuarioModificado.Persona = usuario.Persona;

            Db.SaveChanges();

            usuario = usuarioModificado;
            return usuarioModificado;
        }

        public bool ExisteEmail(string email)
        {
            return Db.Usuarios.Any(usuario => usuario.Email == email && usuario.Activo);
        }

        public Usuario Activar(int id, Guid codigoActivacion)
        {
            var usuario = ObtenerPorId(id);
            if (usuario == null || usuario.CodigoActivacion != codigoActivacion)
                return null;

            usuario.Activo = true;
            Db.SaveChanges();

            return usuario;
        }

        public Usuario CambiarClave(Usuario usuario)
        {
            var usuarioDb = ObtenerPorId(usuario.Id);
            if (usuarioDb == null || usuarioDb.CodigoActivacion != usuario.CodigoActivacion)
                return null;

            usuarioDb.Clave = usuario.Clave;
            usuarioDb.ReiniciarClave = usuario.ReiniciarClave;
            Db.SaveChanges();

            return usuarioDb;
        }

        public override bool Remover(Usuario usuario)
        {
            if (usuario.Persona == null)
                return base.Remover(usuario);

            MarcarEliminado(usuario.Persona);
            Db.SaveChanges();
            return true;
        }
    }
}