﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System;
using System.Collections.Generic;

namespace MiCons.Data.Repositories
{
    public interface IUsuarioRepo : IRepoBase<Usuario>
    {
        Usuario ObtenerPorEmail(string email);
        Usuario ObtenerPorEmailConPersona(string email);
        Usuario Activar(int id, Guid codigoActivacion);
        IEnumerable<Usuario> ObtenerConPersona(ModeloConsulta solicitud = null);
        bool ExisteEmail(string email);
        Usuario CambiarClave(Usuario usuario);
    }
}