﻿using MiCons.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class ComentarioRepo : RepoBase<Comentario>, IComentarioRepo
    {
        public ComentarioRepo(IMiConsDb db) : base(db)
        {
        }

        public IEnumerable<Comentario> ObtenerPorNovedad(int idNovedad)
        {
            return Db.Comentarios.Where(comentario => comentario.IdNovedad == idNovedad).Include(comentario => comentario.Persona).ToList();
        }
    }
}