﻿using MiCons.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class PerfilRepo : RepoBase<Perfil>, IPerfilRepo
    {
        public PerfilRepo(IMiConsDb db) : base(db)
        {
        }

        public Perfil ObtenerPorEmail(string email)
        {
            return (from usuario in Db.Usuarios
                    join persona in Db.Personas on usuario.IdPersona equals persona.Id
                    join empleado in Db.Empleados on persona.Id equals empleado.IdPersona
                    join perfil in Db.Perfiles on empleado.IdPerfil equals perfil.Id
                    where usuario.Email == email
                    select perfil).AsNoTracking().SingleOrDefault();
        }
    }
}