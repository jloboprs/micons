﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public interface IRepoBase<T> where T : class, IEntity
    {
        IQueryable<T> Set { get; set; }
        IEnumerable<T> Obtener(ModeloConsulta consulta = null);
        T ObtenerPorId(int id);
        T Modificar(T entity);
        T Agregar(T entity);
        bool Remover(T entity);
        bool Remover(int id);
    }
}