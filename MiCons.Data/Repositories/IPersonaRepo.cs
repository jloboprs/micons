﻿using MiCons.Data.Entities;
using System.Collections.Generic;

namespace MiCons.Data.Repositories
{
    public interface IPersonaRepo : IRepoBase<Persona>
    {
        IEnumerable<Persona> ObtenerEmpleados();
        Persona ObtenerPorUsuarioId(int idUsuario);
        Persona ObtenerPorDocumento(int idTipoDocumento, string numeroDocumento);
        Persona ObtenerPorEmail(string email);
        Persona Activar(Persona persona);
        bool ExistePersona(int idTipoDocumento, string numeroDocumento);
        bool EsResidente(int idPersona);
        bool EsEmpleado(int idPersona);
        bool EsAdministrador(int idPersona);
    }
}