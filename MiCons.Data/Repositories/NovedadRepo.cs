﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class NovedadRepo : RepoBase<Novedad>, INovedadRepo
    {
        public NovedadRepo(IMiConsDb db) : base(db)
        {
            DefaultOrder = novedad => novedad.Fecha;
        }

        public IEnumerable<Novedad> ObtenerPorAdministrador(int idPersona, ModeloConsulta request)
        {
            Set = IncluirRelaciones(from empleado in Db.Empleados
                                    join novedad in Db.Novedades on empleado.IdConjunto equals novedad.IdConjunto
                                    where empleado.IdPersona == idPersona && empleado.IdPerfil == (int)Perfiles.Administrador
                                    select novedad);

            return Obtener(request);
        }

        public IEnumerable<Novedad> ObtenerPorEmpleado(int idPersona, ModeloConsulta request)
        {
            var perfiles = from empleado in Db.Empleados
                           from novedad in Db.Novedades
                           where empleado.IdPersona == idPersona && novedad.IdConjunto == empleado.IdConjunto && novedad.IdPerfilAsignado == empleado.IdPerfil
                           select novedad.Id;

            Set = IncluirRelaciones(from novedad in Db.Novedades
                                    where novedad.IdPersonaAsignada == idPersona || perfiles.Contains(novedad.Id)
                                    select novedad);

            return Obtener(request);
        }

        public IEnumerable<Novedad> ObtenerPorResidente(int idPersona, ModeloConsulta request)
        {
            Set = IncluirRelaciones(from residente in Db.Residentes
                                    join novedad in Db.Novedades on residente.IdResidencia equals novedad.IdResidencia
                                    where residente.IdPersona == idPersona
                                    select novedad);

            return Obtener(request);
        }

        public override Novedad Modificar(Novedad novedad)
        {
            if (!Exists(novedad.Id))
                return null;

            MarcarCambios(novedad.Comentarios);
            VerificarPersonasIguales(novedad);

            Db.Novedades.Attach(novedad);
            Db.Entry(novedad).State = EntityState.Modified;

            Db.Entry(novedad).OriginalValues["VersionFila"] = novedad.VersionFila;
            novedad.VersionFila = Guid.NewGuid().ToByteArray();

            Db.SaveChanges();

            return novedad;
        }

        public override Novedad Agregar(Novedad novedad)
        {
            novedad.VersionFila = Guid.NewGuid().ToByteArray();
            VerificarPersonasIguales(novedad);
            Db.Novedades.Add(novedad);

            Marcar(novedad.Estado, EntityState.Detached);
            Marcar(novedad.PersonaCreada, EntityState.Detached);
            Marcar(novedad.PersonaAsignada, EntityState.Detached);
            Marcar(novedad.PerfilAsignado, EntityState.Detached);

            Db.SaveChanges();

            return novedad;
        }

        private static void VerificarPersonasIguales(Novedad novedad)
        {
            if (novedad.PersonaCreada != null && novedad.PersonaAsignada != null && novedad.PersonaCreada.Id == novedad.PersonaAsignada.Id)
                novedad.PersonaCreada = novedad.PersonaAsignada;
        }

        private IQueryable<Novedad> IncluirRelaciones(IQueryable<Novedad> novedades)
        {
            return novedades.Include(novedad => novedad.Estado)
                .Include(novedad => novedad.PersonaCreada)
                .Include(novedad => novedad.PersonaAsignada)
                .Include(novedad => novedad.PerfilAsignado)
                .AsNoTracking();
        }
    }
}