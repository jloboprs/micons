﻿using MiCons.Data.Entities;
using System.Collections.Generic;

namespace MiCons.Data.Repositories
{
    public interface IConjuntoRepo : IRepoBase<Conjunto>
    {
        IEnumerable<Conjunto> ObtenerPorPersona(int idPersona);
    }
}