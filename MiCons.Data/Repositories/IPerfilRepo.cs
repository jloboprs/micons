﻿using MiCons.Data.Entities;

namespace MiCons.Data.Repositories
{
    public interface IPerfilRepo : IRepoBase<Perfil>
    {
        Perfil ObtenerPorEmail(string email);
    }
}