﻿using MiCons.Data.Entities;

namespace MiCons.Data.Repositories
{
    public interface IResidenteRepo : IRepoBase<Residente>
    {
    }
}