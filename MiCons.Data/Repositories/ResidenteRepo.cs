﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace MiCons.Data.Repositories
{
    public class ResidenteRepo : RepoBase<Residente>, IResidenteRepo
    {
        public ResidenteRepo(IMiConsDb db) : base(db)
        {
        }

        public override IEnumerable<Residente> Obtener(ModeloConsulta request = null)
        {
            Set = Db.Residentes.Include(residente => residente.Residencia);

            return base.Obtener(request);
        }
    }
}