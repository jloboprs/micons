﻿using MiCons.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MiCons.Data.Repositories
{
    public class ConjuntoRepo : RepoBase<Conjunto>, IConjuntoRepo
    {
        public ConjuntoRepo(IMiConsDb db) : base(db)
        {
        }

        public IEnumerable<Conjunto> ObtenerPorPersona(int idPersona)
        {
            var residencias = (from residente in Db.Residentes
                            join residencia in Db.Residencias on residente.IdResidencia equals residencia.Id
                            where residente.IdPersona == idPersona
                            select residencia).Include(residencia => residencia.Conjunto).ToList();

            return residencias.Select(itm => itm.Conjunto).Distinct();
        }
    }
}