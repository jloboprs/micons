﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace MiCons.Data.Repositories
{
    public class RepoBase<T> : IRepoBase<T> where T : class, IEntity, new()
    {
        public Expression<Func<T, object>> DefaultOrder;

        public bool IsPageable { get; set; }

        public bool IsAscending { get; set; }

        public int DefaultTake { get; set; }

        protected IMiConsDb Db { get; private set; }

        public virtual IQueryable<T> Set { get; set; }

        public RepoBase(IMiConsDb db)
        {
            Db = db;

            IsPageable = false;
            IsAscending = true;
            DefaultTake = 10;
            DefaultOrder = entity => entity.Id;
        }

        public virtual IEnumerable<T> Obtener(ModeloConsulta request = null)
        {
            var query = Set ?? Db.Set<T>();

            if (request != null && request.Filters != null && request.Filters.Any())
                query = query.Where(request.Filters);

            query = request != null && !string.IsNullOrWhiteSpace(request.OrderBy)
                ? query.OrderBy(request.OrderBy, request.Ascending)
                : query.OrderBy(DefaultOrder, IsAscending);

            if (IsPageable && (request == null || request.IsPageable))
            {
                query = request == null || request.Take <= 0
                        ? query.Take(DefaultTake)
                        : query.Take(request.Take);

                if (request != null && request.Page > 1)
                    query = query.Skip(request.Skip);
            }

            return query.ToList();
        }

        public virtual T ObtenerPorId(int id)
        {
            return Db.Set<T>().FirstOrDefault(entity => entity.Id == id);
        }

        public virtual T Modificar(T entity)
        {
            if (!Exists(entity.Id))
                return null;

            Db.Set<T>().Attach(entity);
            Db.Entry(entity).State = EntityState.Modified;
            Db.SaveChanges();

            return entity;
        }

        public virtual T Agregar(T entity)
        {
            Db.Set<T>().Add(entity);
            Db.SaveChanges();

            return entity;
        }

        public virtual bool Remover(T entity)
        {
            if (!Exists(entity.Id))
                return false;

            MarcarEliminado(entity);
            Db.SaveChanges();
            return true;
        }

        public virtual bool Remover(int id)
        {
            if (!Exists(id))
                return false;

            Db.Entry(new T { Id = id }).State = EntityState.Deleted;
            Db.SaveChanges();
            return true;
        }

        public virtual bool Exists(int id)
        {
            return Db.Set<T>().Any(entity => entity.Id == id);
        }

        protected void MarcarCambio<Y>(Y entity) where Y : class, IEntity
        {
            Marcar(entity, entity.Id > 0 ? EntityState.Modified : EntityState.Added);
        }

        protected void Marcar<Y>(Y entity, EntityState estado) where Y : class, IEntity
        {
            if (entity == null)
                return;

            Db.Entry(entity).State = estado;
        }

        protected void MarcarEliminado<Y>(Y entity) where Y : class
        {
            if (entity == null)
                return;

            Db.Entry(entity).State = EntityState.Deleted;
        }

        protected void MarcarCambios<Y>(IEnumerable<Y> entities) where Y : class, IEntity
        {
            if (entities != null)
                foreach (var entity in entities)
                    MarcarCambio(entity);
        }
    }
}