namespace MiCons.Data
{
    public enum Perfiles { Administrador = 1, Residente = 2 }
    public enum TipoDocumentos { CC = 1, RegistroCivil = 2, Pasaporte = 3, CE = 4 }
    public enum EstadoNovedad { Abierto = 1, EnCurso = 2, cerrado = 3 }
}