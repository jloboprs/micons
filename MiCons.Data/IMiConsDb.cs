using MiCons.Data.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MiCons.Data
{
    public interface IMiConsDb : IDisposable
    {
        DbSet<Comentario> Comentarios { get; set; }
        DbSet<Conjunto> Conjuntos { get; set; }
        DbSet<Empleado> Empleados { get; set; }
        DbSet<Estado> Estados { get; set; }
        DbSet<Novedad> Novedades { get; set; }
        DbSet<Perfil> Perfiles { get; set; }
        DbSet<Persona> Personas { get; set; }
        DbSet<Residencia> Residencias { get; set; }
        DbSet<Residente> Residentes { get; set; }
        DbSet<TipoDocumento> TipoDocumentos { get; set; }
        DbSet<Usuario> Usuarios { get; set; }

        DbSet<T> Set<T>() where T : class;

        DbEntityEntry<T> Entry<T>(T entity) where T : class;

        int SaveChanges();
    }
}