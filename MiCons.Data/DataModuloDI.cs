﻿using Autofac;
using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using System.Data.Entity;

namespace MiCons.Data
{
    public class DataModuloDI : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MiConsDb>()
                .As<DbContext>()
                .As<IMiConsDb>()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(RepoBase<>))
                .As(typeof(IRepoBase<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<NovedadRepo>()
                .As<INovedadRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UsuarioRepo>()
                .As<IUsuarioRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PersonaRepo>()
                .As<IPersonaRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PerfilRepo>()
                .As<IPerfilRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ResidenteRepo>()
                .As<IResidenteRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ConjuntoRepo>()
                .As<IConjuntoRepo>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ComentarioRepo>()
                .As<IComentarioRepo>()
                .InstancePerLifetimeScope();
        }
    }
}