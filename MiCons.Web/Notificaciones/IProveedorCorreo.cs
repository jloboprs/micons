﻿using System.Net.Mail;

namespace MiCons.Web.Notificaciones
{
    public interface IProveedorCorreo
    {
        SmtpClient ObtenerCliente();
        MailAddress ObtenerRemitente();
    }
}