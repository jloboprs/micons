﻿using MiCons.Data.Entities;
using MiCons.Web.Helpers;
using System.Linq;
using System.Text;

namespace MiCons.Web.Notificaciones
{
    public class NotificadorUsuario : INotificador
    {
        IClienteCorreo _cliente;

        private string UrlDominio
        {
            get
            {
                var url = ContextValue.Url.Content("~", true);
                if (url.Last() != '/')
                    return url;

                return url.Remove(url.Length - 1);
            }
        }

        public NotificadorUsuario(IClienteCorreo cliente)
        {
            _cliente = cliente;
        }

        //TODO: Cambiar el cuerpo por un template de Razor
        public void Notificar(Usuario usuario)
        {
            var sujeto = string.Format("Confirma tu usuario en {0}", ContextValue.NombreAplicacion);

            var mensaje = new StringBuilder();

            if (usuario.ReiniciarClave)
                mensaje.AppendFormat(@"<h2>{0}</h2><br/><br/>Se solicitó que reinicio de tu clave. Debe seguir el siguiente enlace para cambiarla:", ContextValue.NombreAplicacion);
            else
                mensaje.AppendFormat(@"<h2>{0}</h2><br/><br/>Gracias por registrarte en {0}! Debe seguir este enlace para activar su cuenta:", ContextValue.NombreAplicacion);

            mensaje.AppendFormat(@"<br/><br/><a href=""{0}"" target=""_blank"">{0}</a>", ObtenerUrlActivacion(usuario));
            mensaje.AppendFormat(@"<br/><br/>El equipo {0}", ContextValue.NombreAplicacion);
            mensaje.AppendFormat(@"<br/><a href=""{0}"" target=""_blank"">{0}</a>", UrlDominio);

            _cliente.Enviar(sujeto, mensaje.ToString(), ObtenerEmail(usuario));
        }

        private string ObtenerUrlActivacion(Usuario usuario)
        {
            return ContextValue.Url.Content(string.Format("~/usuario/activar/{0}/{1}/{2}", usuario.CodigoActivacion, usuario.Id, usuario.ReiniciarClave), true);
        }

        private string ObtenerEmail(Usuario usuario)
        {
            if (usuario.Persona != null)
                return string.Format("{0} <{1}>", usuario.Persona.Nombre, usuario.Email);

            return usuario.Email;
        }
    }
}