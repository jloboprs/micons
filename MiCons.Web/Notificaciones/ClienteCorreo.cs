﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace MiCons.Web.Notificaciones
{
    public class ClienteCorreo : IClienteCorreo
    {
        IProveedorCorreo _proveedor;

        public ClienteCorreo(IProveedorCorreo proveedor)
        {
            _proveedor = proveedor;
        }

        public void Enviar(string sujeto, string cuerpo, params string[] destinos)
        {
            var mensaje = GenerarMensaje(sujeto, cuerpo, destinos);
            var cliente = _proveedor.ObtenerCliente();

            Task.Run(() =>
            {
                using (mensaje)
                {
                    using (cliente)
                    {
                        cliente.Send(mensaje);
                    }
                }
            });
        }

        private MailMessage GenerarMensaje(string subject, string body, string[] destinos)
        {
            var mensaje = new MailMessage
            {
                From = _proveedor.ObtenerRemitente(),
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };

            foreach (var destino in destinos)
                mensaje.To.Add(destino);

            return mensaje;
        }
    }
}