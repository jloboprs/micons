﻿using MiCons.Data.Entities;

namespace MiCons.Web.Notificaciones
{
    public interface INotificador
    {
        void Notificar(Usuario usuario);
    }
}