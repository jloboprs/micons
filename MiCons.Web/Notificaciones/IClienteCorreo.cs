﻿namespace MiCons.Web.Notificaciones
{
    public interface IClienteCorreo
    {
        void Enviar(string sujeto, string cuerpo, params string[] destinos);
    }
}