﻿using MiCons.Web.Helpers;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Web;

namespace MiCons.Web.Notificaciones
{
    public class ProveedorCorreo : IProveedorCorreo
    {
        public string Host
        {
            get { return ConfigurationManager.AppSettings["Email.Host"]; }
        }

        public int Puerto
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Email.Puerto"]); }
        }

        public string Usuario
        {
            get { return ConfigurationManager.AppSettings["Email.Usuario"]; }
        }

        public string Email
        {
            get
            {
                if (string.IsNullOrEmpty(Sobrenombre))
                    return Usuario;

                return string.Format("{0} <{1}>", Sobrenombre, Usuario);
            }
        }

        private string Sobrenombre
        {
            get { return ContextValue.NombreAplicacion; }
        }

        public SecureString Clave
        {
            get
            {
                var clave = new SecureString();
                foreach (var letter in ConfigurationManager.AppSettings["Email.Clave"])
                    clave.AppendChar(letter);

                clave.MakeReadOnly();
                return clave;
            }
        }

        public SmtpClient ObtenerCliente()
        {
            return new SmtpClient(Host, Puerto)
            {
                Credentials = new NetworkCredential(Usuario, Clave),
                EnableSsl = true
            };
        }

        public MailAddress ObtenerRemitente()
        {
            return new MailAddress(Email);
        }
    }
}