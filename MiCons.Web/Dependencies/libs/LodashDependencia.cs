using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.libs
{

    public class LodashDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.None; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield break;
        }
    }
}