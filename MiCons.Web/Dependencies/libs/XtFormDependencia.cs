using MiCons.Web.Dependencies.Configurations;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.libs
{

    public class XtFormDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Module; }
        }

        public override string NombreInyeccion
        {
            get { return "xtForm"; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(XtFormConfigDependencia);
        }
    }
}