using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.libs
{

    public class AngularCookiesDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Module; }
        }

        public override string NombreInyeccion
        {
            get { return "ngCookies"; }
        }

        public override string NombreArchivo
        {
            get { return "angular-cookies"; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
        }
    }
}