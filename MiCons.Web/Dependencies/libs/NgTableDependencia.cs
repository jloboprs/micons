using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.libs
{

    public class NgTableDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Module; }
        }

        public override string NombreInyeccion
        {
            get { return "ngTable"; }
        }

        public override string NombreArchivo
        {
            get { return "ng-table"; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
        }
    }
}