using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Helpers
{

    public class AlertDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.None; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield break;
        }
    }
}