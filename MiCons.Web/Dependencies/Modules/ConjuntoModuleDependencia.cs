using MiCons.Web.Dependencies.Controllers;
using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Modules
{

    public class ConjuntoModuleDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Module; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(AutenticacionControllerDependencia);
            yield return typeof(ConjuntoControllerDependencia);
        }
    }
}