using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.libs;
using MiCons.Web.Dependencies.Validations;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class ActivacionUsuarioControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(UsuarioApiDependencia);
            yield return typeof(NgEqualsValidationDependencia);
            yield return typeof(XtFormDependencia);
        }
    }
}