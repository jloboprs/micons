using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.libs;
using MiCons.Web.Dependencies.Services;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class NovedadControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(NovedadApiDependencia);
            yield return typeof(ConjuntoApiDependencia);
            yield return typeof(ResidenciaApiDependencia);
            yield return typeof(SesionServiceDependencia);
            yield return typeof(ComentarioApiDependencia);
            yield return typeof(XtFormDependencia);
        }
    }
}