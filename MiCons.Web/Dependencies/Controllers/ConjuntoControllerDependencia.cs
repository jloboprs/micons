using MiCons.Web.Dependencies.Api;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class ConjuntoControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(ConjuntoApiDependencia);
            yield return typeof(ResidenciaApiDependencia);
            yield return typeof(TablaControllerDependencia);
        }
    }
}