using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.Helpers;
using MiCons.Web.Dependencies.libs;
using MiCons.Web.Dependencies.Validations;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class PersonaControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(UsuarioApiDependencia);
            yield return typeof(PersonaApiDependencia);
            yield return typeof(EmpleadoApiDependencia);
            yield return typeof(ResidenteApiDependencia);
            yield return typeof(ResidenciaApiDependencia);
            yield return typeof(TablaControllerDependencia);
            yield return typeof(NgEmailUnicoValidationDependencia);
            yield return typeof(NgDocumentoUnicoValidationDependencia);
            yield return typeof(XtFormDependencia);
        }
    }
}