using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.Helpers;
using MiCons.Web.Dependencies.libs;
using MiCons.Web.Dependencies.Validations;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class UsuarioControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AlertDependencia);
            yield return typeof(UsuarioApiDependencia);
            yield return typeof(NgEmailUnicoValidationDependencia);
            yield return typeof(NgDocumentoUnicoValidationDependencia);
            yield return typeof(XtFormDependencia);
        }
    }
}