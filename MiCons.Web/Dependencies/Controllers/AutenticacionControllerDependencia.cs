using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.Directives;
using MiCons.Web.Dependencies.Helpers;
using MiCons.Web.Dependencies.Services;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class AutenticacionControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AlertDependencia);
            yield return typeof(UsuarioApiDependencia);
            yield return typeof(SesionServiceDependencia);
            yield return typeof(NgRemoveClassDirectiveDependencia);
        }
    }
}