using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Controllers
{

    public class TablaControllerDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Controller; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(LodashDependencia);
            yield return typeof(NgTableDependencia);
        }
    }
}