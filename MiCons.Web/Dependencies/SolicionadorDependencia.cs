﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MiCons.Web.Dependencies
{
    public static class SolicionadorDependencia
    {
        public static IEnumerable<DependenciaBase> ObtenerDependenciasAngular(DependenciaBase dependencia)
        {
            return ObtenerDependencias(dependencia, new List<Type>());
        }

        public static string[] ObtenerDependenciasBundle(DependenciaBase dependencia)
        {
            return ObtenerDependencias(dependencia, new List<Type>()).Select(dependenciaJs => dependenciaJs.RutaArchivo).ToArray();
        }

        static IEnumerable<DependenciaBase> ObtenerDependencias(DependenciaBase dependencia, ICollection<Type> archivos)
        {
            if (dependencia == null)
                yield break;

            foreach (var dependenciaHijo in ObtenerDependenciasHijos(dependencia, archivos))
                foreach (var rutaHijo in ObtenerDependencias(dependenciaHijo, archivos))
                   yield return rutaHijo;

            yield return dependencia;
        }

        static IEnumerable<DependenciaBase> ObtenerDependenciasHijos(DependenciaBase dependencia, ICollection<Type> archivos)
        {
            foreach (var dependenciaHijo in dependencia.ObtenerDependencias())
            {
                if (dependenciaHijo.IsSubclassOf(typeof(DependenciaBase)) && !archivos.Contains(dependenciaHijo))
                {
                    archivos.Add(dependenciaHijo);
                    yield return Activator.CreateInstance(dependenciaHijo) as DependenciaBase;
                }
            }
        }
    }
}
