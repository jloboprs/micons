using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Services
{

    public class SesionServiceDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Service; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularCookiesDependencia);
        }
    }
}