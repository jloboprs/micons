using MiCons.Web.Dependencies.Helpers;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Services
{

    public class DefaultInterceptorDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Service; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AlertDependencia);
        }
    }
}