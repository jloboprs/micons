using MiCons.Web.Dependencies.Configurations;
using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Api
{

    public class ApiBaseDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.None; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(HttpConfigDependencia);
        }
    }
}