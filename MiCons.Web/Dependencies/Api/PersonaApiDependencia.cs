using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Api
{

    public class PersonaApiDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Service; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(ApiBaseDependencia);
        }
    }
}