using MiCons.Web.Dependencies.libs;
using MiCons.Web.Dependencies.Services;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Configurations
{

    public class HttpConfigDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Config; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(DefaultInterceptorDependencia);
        }
    }
}