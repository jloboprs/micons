using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Configurations
{

    public class XtFormConfigDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Config; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield break;
        }
    }
}