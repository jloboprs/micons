﻿using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies
{
    public abstract class DependenciaBase
    {
        public enum EnumTipoObjeto { Directive, Service, Controller, Config, Module, None }

        const string nombreBase = "Dependencia";
        protected static Type _tipoBase = typeof(DependenciaBase);
        protected Type _tipo;

        public DependenciaBase()
        {
            _tipo = GetType();
        }

        string _rutaArchivo;
        public virtual string RutaArchivo
        {
            get
            {
                if (_rutaArchivo != null)
                    return _rutaArchivo;

                var indexNamespace = _tipo.Namespace.IndexOf(_tipoBase.Namespace);
                var namespaceExtra = indexNamespace != -1 ? _tipo.Namespace.Remove(indexNamespace, _tipoBase.Namespace.Length) : string.Empty;

                return _rutaArchivo = string.Format("~/Scripts{0}/{1}.js", namespaceExtra.Replace('.', '/'), NombreArchivo);
            }
        }

        string _nombreFuncion;
        public virtual string NombreFuncion
        {
            get
            {
                if (_nombreFuncion != null)
                    return _nombreFuncion;

                return _nombreFuncion = ObtenerNombreJs();
            }
        }

        string _nombreArchivo;
        public virtual string NombreArchivo
        {
            get
            {
                if (_nombreArchivo != null)
                    return _nombreArchivo;

                return _nombreArchivo = ObtenerNombreJs();
            }
        }

        string _nombreInyeccion;
        public virtual string NombreInyeccion
        {
            get
            {
                if (_nombreInyeccion != null)
                    return _nombreInyeccion;

                return _nombreInyeccion = char.ToLower(NombreFuncion[0]) + NombreFuncion.Substring(1);
            }
        }

        public abstract IEnumerable<Type> ObtenerDependencias();

        public abstract EnumTipoObjeto TipoObjeto { get; }

        public virtual bool EsModulo
        {
            get { return false; }
        }

        protected string ObtenerNombreJs()
        {
            var index = _tipo.Name.IndexOf(nombreBase);
            return index != -1 ? _tipo.Name.Remove(index, nombreBase.Length) : _tipo.Name;
        }
    }
}