using MiCons.Web.Dependencies.Api;
using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Validations
{

    public class NgEmailUnicoValidationDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Directive; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
            yield return typeof(UsuarioApiDependencia);
        }
    }
}