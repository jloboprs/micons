using MiCons.Web.Dependencies.libs;
using System;
using System.Collections.Generic;

namespace MiCons.Web.Dependencies.Validations
{

    public class NgEqualsValidationDependencia : DependenciaBase
    {
        public override EnumTipoObjeto TipoObjeto
        {
            get { return EnumTipoObjeto.Directive; }
        }

        public override IEnumerable<Type> ObtenerDependencias()
        {
            yield return typeof(AngularDependencia);
        }
    }
}