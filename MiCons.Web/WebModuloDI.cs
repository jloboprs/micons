﻿using Autofac;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using MiCons.Web.Mappers;
using MiCons.Web.Notificaciones;
using System.Security.Cryptography;

namespace MiCons.Data
{
    public class WebModuloDI : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(ApiAutorizacionBase<>))
                .As(typeof(IApiAutorizacionBase<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<ApiNovedadAutorizacion>()
                .As<IApiNovedadAutorizacion>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ApiPersonaAutorizacion>()
                .As<IApiPersonaAutorizacion>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ApiUsuarioAutorizacion>()
                .As<IApiUsuarioAutorizacion>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ApiPerfilAutorizacion>()
                .As<IApiPerfilAutorizacion>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProveedorCorreo>()
                .As<IProveedorCorreo>()
                .SingleInstance();

            builder.RegisterType<ClienteCorreo>()
                .As<IClienteCorreo>()
                .SingleInstance();

            builder.RegisterType<NotificadorUsuario>()
                .As<INotificador>()
                .SingleInstance();

            builder.RegisterType<VariablesSesion>()
                .As<IVariablesSesion>()
                .SingleInstance();

            builder.RegisterType<UsuarioSesionModelMapper>()
                .As<IUsuarioSesionModelMapper>()
                .SingleInstance();

            builder.RegisterType<GeneradorHash>()
                .As<IGeneradorHash>()
                .SingleInstance();

            builder.RegisterType<SHA512Managed>()
                .As<HashAlgorithm>()
                .SingleInstance();

            builder.RegisterType<RNGCryptoServiceProvider>()
                .As<RandomNumberGenerator>()
                .SingleInstance();
        }
    }
}