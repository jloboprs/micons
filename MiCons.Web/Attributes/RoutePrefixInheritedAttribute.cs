﻿using System;
using System.Linq;
using System.Web.Mvc.Routing;

namespace MiCons.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class RoutePrefixInheritedAttribute : Attribute, IDirectRouteFactory
    {
        public string Name { get; set; }

        public int Order { get; set; }

        public string Template { get; private set; }

        public RoutePrefixInheritedAttribute(string template)
        {
            Template = template;
        }

        public RouteEntry CreateRoute(DirectRouteFactoryContext context)
        {
            var controllerDescriptor = context.Actions.First().ControllerDescriptor;

            var template = Template.Replace("{controller}", controllerDescriptor.ControllerName);

            var builder = context.CreateBuilder(template);

            builder.Name = Name;
            builder.Order = Order;

            return builder.Build();
        }
    }
}
