﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace MiCons.Web.Providers
{
    public abstract class JsonNetValueProviderFactoryBase : ValueProviderFactory
    {
        JsonSerializer _serializer;

        protected DictionaryValueProviderConverter Converter { get; private set; }
        protected JsonTextReader Reader { get; private set; }
        protected HttpRequestBase Request { get; private set; }

        protected JsonSerializer Serializer
        {
            get
            {
                if (_serializer == null)
                {
                    _serializer = new JsonSerializer();
                    _serializer.Converters.Add(new ExpandoObjectConverter());
                }

                return _serializer;
            }
        }

        public JsonNetValueProviderFactoryBase()
        {
            Converter = new DictionaryValueProviderConverter();
        }

        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");

            Request = controllerContext.HttpContext.Request;

            if (!Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                return null;

            return OnGetValueProvider();
        }

        protected abstract IValueProvider OnGetValueProvider();

        protected void CreateReader(Stream input)
        {
            Reader = new JsonTextReader(new StreamReader(input));
        }

        protected void CreateReader(string input)
        {
            Reader = new JsonTextReader(new StringReader(input));
            Reader.Read();
        }

        protected object Deserialize()
        {
            if (Reader.TokenType == JsonToken.StartArray)
                return Serializer.Deserialize<List<ExpandoObject>>(Reader);

            if (Reader.TokenType == JsonToken.StartObject)
                return Serializer.Deserialize<ExpandoObject>(Reader);

            return Reader.Value;
        }
    }
}