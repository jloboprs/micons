﻿using MiCons.Web.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Routing;

namespace MiCons.Web.Providers
{
    public class RoutePrefixInheritedProvider : DefaultDirectRouteProvider
    {
        protected override IReadOnlyList<IDirectRouteFactory> GetControllerRouteFactories(ControllerDescriptor controllerDescriptor)
        {
            return controllerDescriptor.GetCustomAttributes(typeof(RoutePrefixInheritedAttribute), inherit: true).Cast<IDirectRouteFactory>().ToArray();
        }

        protected override IReadOnlyList<IDirectRouteFactory> GetActionRouteFactories(ActionDescriptor actionDescriptor)
        {
            return actionDescriptor.GetCustomAttributes(typeof(IDirectRouteFactory), inherit: true).Cast<IDirectRouteFactory>().ToArray();
        }
    }
}