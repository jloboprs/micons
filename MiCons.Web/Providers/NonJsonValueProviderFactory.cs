﻿using System;
using System.Web.Mvc;

namespace MiCons.Web.Providers
{
    internal class NonJsonValueProviderFactory : ValueProviderFactory
    {
        ValueProviderFactory _factory;

        public NonJsonValueProviderFactory(ValueProviderFactory factory)
        {
            _factory = factory;
        }

        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");

            if (controllerContext.HttpContext.Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                return null;

            return _factory.GetValueProvider(controllerContext);
        }
    }
}