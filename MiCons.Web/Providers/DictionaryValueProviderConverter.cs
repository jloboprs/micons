﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace MiCons.Web.Providers
{
    public class DictionaryValueProviderConverter
    {
        public DictionaryValueProvider<object> Convert(object json)
        {
            var data = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            AddData(data, String.Empty, json);

            return new DictionaryValueProvider<object>(data, CultureInfo.CurrentCulture);
        }

        public void AddData(Dictionary<string, object> data, string prefix, object value)
        {
            if (AddAsList(data, prefix, value))
                return;

            if (AddAsDictionary(data, prefix, value))
                return;

            AddAsValue(data, prefix, value);
        }

        private bool AddAsList(Dictionary<string, object> data, string prefix, object value)
        {
            var list = value as IList;
            if (list == null)
                return false;

            if (list.Count <= 0)
            {
                AddData(data, prefix, null);
                return true;
            }

            for (var i = 0; i < list.Count; i++)
                AddData(data, MakeArrayKey(prefix, i), list[i]);

            return true;
        }

        private bool AddAsDictionary(Dictionary<string, object> data, string prefix, object value)
        {
            var dictionary = value as IDictionary<string, object>;
            if (dictionary == null)
                return false;

            var i = 0;
            foreach (var entry in dictionary)
            {
                var newPrefix = MakeArrayKey(prefix, i);

                AddData(data, newPrefix + ".key", entry.Key);
                AddData(data, newPrefix + ".value", entry.Value);
                AddData(data, MakePropertyKey(prefix, entry.Key), entry.Value);
                i++;
            }

            return true;
        }

        private void AddAsValue(Dictionary<string, object> data, string prefix, object value)
        {
            data[prefix] = value;
        }

        private static string MakeArrayKey(string prefix, int index)
        {
            return prefix + "[" + index.ToString(CultureInfo.InvariantCulture) + "]";
        }

        private static string MakePropertyKey(string prefix, string propertyName)
        {
            return String.IsNullOrEmpty(prefix) ? propertyName : prefix + "." + propertyName;
        }
    }
}