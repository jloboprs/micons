﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace MiCons.Web.Providers
{
    public class JsonNetUrlValueProviderFactory : JsonNetValueProviderFactoryBase
    {
        protected override IValueProvider OnGetValueProvider()
        {
            if (Request.QueryString.Count <= 0)
                return null;

            var data = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            foreach (string key in Request.QueryString)
            {
                var value = Request.QueryString[key];
                if (string.IsNullOrWhiteSpace(value))
                    continue;

                CreateReader(value);

                var json = Deserialize();

                Converter.AddData(data, key, json);
            }

            return new DictionaryValueProvider<object>(data, CultureInfo.CurrentCulture);
        }
    }
}
