﻿using MiCons.Data;
using MiCons.Data.Repositories;
using MiCons.Web.Helpers;
using MiCons.Web.Mappers;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Security;

namespace MiCons.Web.Providers
{
    public static class AutenticacionRequestProvider
    {
        const string _rutaCookie = "/";
        const string _nombreJsCookie = ".JSXAUTH";

        static HttpCookie PerfilesCookie { get { return ContextValue.ObtenerCookie(FormsAuthentication.FormsCookieName); } }

        static DateTime Expiracion { get { return PerfilesCookie != null ? PerfilesCookie.Expires : DateTime.MinValue; } }

        static bool EsPersistente { get { return Expiracion == DateTime.MinValue; } }

        static void AsignarPerfil(string perfil)
        {
            var ticket = new FormsAuthenticationTicket(1, ContextValue.UserName, DateTime.Now, Expiracion, EsPersistente, perfil, _rutaCookie);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
            {
                Value = FormsAuthentication.Encrypt(ticket),
                Expires = Expiracion
            };

            ContextValue.AsignarCookie(cookie);
        }

        static string ObtenerPerfil()
        {
            if (PerfilesCookie == null || PerfilesCookie.Value == null)
                return null;

            var perfil = FormsAuthentication.Decrypt(PerfilesCookie.Value).UserData;
            if (string.IsNullOrEmpty(perfil))
                return null;

            return perfil;
        }

        static object SesionJs
        {
            get
            {
                var cookie = ContextValue.ObtenerCookie(_nombreJsCookie);
                if (cookie == null)
                    return null;

                return cookie.Value;
            }
            set
            {
                var cookie = new HttpCookie(_nombreJsCookie)
                {
                    Value = JsonConvert.SerializeObject(value),
                    Expires = value != null ? Expiracion : DateTime.Now.AddDays(-1)
                };

                ContextValue.AsignarCookie(cookie);
            }
        }

        public static void AsignarAutenticacion()
        {
            if (!ContextValue.EstaAutenticado && SesionJs != null)
            {
                SesionJs = null;
                return;
            }

            if (!ContextValue.EstaAutenticado || SesionJs != null)
                return;

            using (var db = new MiConsDb())
            {
                var mapper = new UsuarioSesionModelMapper();
                var repo = new PersonaRepo(db);

                var persona = repo.ObtenerPorEmail(ContextValue.UserName);
                var esResidente = repo.EsResidente(persona.Id);
                var esAdministrador = repo.EsAdministrador(persona.Id);
                var esEmpleado = esAdministrador || repo.EsEmpleado(persona.Id);

                var usuario = mapper.Mapear(persona, ContextValue.UserName, esResidente, esEmpleado, esAdministrador);
                SesionJs = usuario;
            }
        }
    }
}