﻿using System.Web.Mvc;

namespace MiCons.Web.Providers
{
    public class JsonNetValueProviderFactory : JsonNetValueProviderFactoryBase
    {
        protected override IValueProvider OnGetValueProvider()
        {
            CreateReader(Request.InputStream);

            if (!Reader.Read())
                return null;

            var json = Deserialize();
            return json != null ? Converter.Convert(json) : null;
        }
    }
}