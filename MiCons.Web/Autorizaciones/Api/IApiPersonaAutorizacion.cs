﻿using System.Web.Mvc;
using MiCons.Data.Entities;

namespace MiCons.Web.Autorizaciones.Api
{
    public interface IApiPersonaAutorizacion : IApiAutorizacionBase<Persona>
    {
        ActionResult DenegarActivar(Persona persona);
    }
}