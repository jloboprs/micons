﻿using System.Web.Mvc;
using MiCons.Data.Entities;
using MiCons.Web.Helpers;

namespace MiCons.Web.Autorizaciones.Api
{
    public class ApiUsuarioAutorizacion : ApiAutorizacionBase<Usuario>, IApiUsuarioAutorizacion
    {
        public ApiUsuarioAutorizacion(IVariablesSesion sesion) : base(sesion)
        {
        }

        public override ActionResult DenegarModificar(Usuario usuario)
        {
            var denegado = base.DenegarModificar(usuario);
            if (denegado != null)
                return denegado;

            if (usuario.IdPersona <= 0)
                return HttpBadRequest();

            return null;
        }

        public override ActionResult DenegarAgregar(Usuario usuario)
        {
            if (usuario == null)
                return HttpBadRequest();

            return null;
        }

        public ActionResult DenegarAutenticar(Usuario usuario, bool persistente)
        {
            return usuario == null ? HttpBadRequest() : null;
        }

        public ActionResult DenegarCambiarClave(Usuario usuario)
        {
            return usuario == null ? HttpBadRequest() : null;
        }
    }
}