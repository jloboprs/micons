﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Autorizaciones.Api
{
    public class ApiNovedadAutorizacion : ApiAutorizacionBase<Novedad>, IApiNovedadAutorizacion
    {
        public ApiNovedadAutorizacion(IVariablesSesion sesion) : base(sesion)
        {
        }

        public override ActionResult DenegarObtener(ModeloConsulta solicitud)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return base.DenegarObtener(solicitud);
        }

        public override ActionResult DenegarObtenerPorId(int? id)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return base.DenegarObtenerPorId(id);
        }
    }
}