﻿using MiCons.Data;
using MiCons.Data.Entities;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Autorizaciones.Api
{
    public class ApiPersonaAutorizacion : ApiAutorizacionBase<Persona>, IApiPersonaAutorizacion
    {
        public ApiPersonaAutorizacion(IVariablesSesion sesion) : base(sesion)
        {
        }

        public override ActionResult DenegarModificar(Persona persona)
        {
            var denegado = base.DenegarModificar(persona);
            if (denegado != null)
                return denegado;

            if (persona.Id != Sesion.Usuario.IdPersona && !Sesion.Usuario.EsAdministrador)
                return HttpForbidden();

            return null;
        }

        public ActionResult DenegarActivar(Persona persona)
        {
            if (!EstaAutenticado || !Sesion.Usuario.EsAdministrador)
                return HttpForbidden();

            if (persona == null)
                return HttpBadRequest();

            return null;
        }
    }
}