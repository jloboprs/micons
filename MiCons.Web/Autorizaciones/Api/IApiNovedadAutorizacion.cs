﻿using MiCons.Data.Entities;

namespace MiCons.Web.Autorizaciones.Api
{
    public interface IApiNovedadAutorizacion : IApiAutorizacionBase<Novedad>
    {
    }
}