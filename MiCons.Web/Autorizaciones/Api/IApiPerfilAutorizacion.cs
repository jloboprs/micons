﻿using System.Web.Mvc;
using MiCons.Data.Entities;

namespace MiCons.Web.Autorizaciones.Api
{
    public interface IApiPerfilAutorizacion : IApiAutorizacionBase<Perfil>
    {
    }
}