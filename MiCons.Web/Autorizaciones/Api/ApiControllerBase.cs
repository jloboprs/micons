﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using MiCons.Web.Helpers;
using System.Net;
using System.Web.Mvc;

namespace MiCons.Web.Autorizaciones.Api
{
    //TODO: Cambiar el nombre del archivo por el que le corresponde
    public class ApiAutorizacionBase<T> : Controller, IApiAutorizacionBase<T> where T : class, IEntity
    {
        protected IVariablesSesion Sesion { get; set; }

        public bool EstaAutenticado
        {
            get { return ContextValue.EstaAutenticado; }
        }

        public ApiAutorizacionBase(IVariablesSesion sesion)
        {
            Sesion = sesion;
        }

        public virtual ActionResult DenegarObtener(ModeloConsulta solicitud)
        {
            return null;
        }

        public virtual ActionResult DenegarObtenerPorId(int? id)
        {
            return !id.HasValue ? HttpBadRequest() : null;
        }

        public virtual ActionResult DenegarAgregar(T data)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return data == null ? HttpBadRequest() : null;
        }

        public virtual ActionResult DenegarModificar(T data)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return data == null ? HttpBadRequest() : null;
        }

        public virtual ActionResult DenegarRemover(T data)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return data == null ? HttpBadRequest() : null;
        }

        public virtual ActionResult DenegarRemoverPorId(int? id)
        {
            if (!EstaAutenticado)
                return HttpUnauthorized();

            return !id.HasValue ? HttpBadRequest() : null;
        }

        protected ActionResult HttpUnauthorized()
        {
            //TODO: Se retorna Forbidden porque mvc responde mal con Unauthorized
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        protected ActionResult HttpForbidden()
        {
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        protected ActionResult HttpBadRequest()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}