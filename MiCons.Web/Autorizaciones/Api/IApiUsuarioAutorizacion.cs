﻿using System.Web.Mvc;
using MiCons.Data.Entities;

namespace MiCons.Web.Autorizaciones.Api
{
    public interface IApiUsuarioAutorizacion : IApiAutorizacionBase<Usuario>
    {
        ActionResult DenegarAutenticar(Usuario usuario, bool persistente);
        ActionResult DenegarCambiarClave(Usuario usuario);
    }
}