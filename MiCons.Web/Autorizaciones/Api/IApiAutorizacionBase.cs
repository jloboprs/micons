﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using System.Web.Mvc;

namespace MiCons.Web.Autorizaciones.Api
{
    public interface IApiAutorizacionBase<T> where T : class, IEntity
    {
        bool EstaAutenticado { get; }
        ActionResult DenegarAgregar(T data);
        ActionResult DenegarModificar(T data);
        ActionResult DenegarObtener(ModeloConsulta solicitud);
        ActionResult DenegarObtenerPorId(int? id);
        ActionResult DenegarRemover(T data);
        ActionResult DenegarRemoverPorId(int? id);
    }
}