﻿using MiCons.Data;
using MiCons.Data.Entities;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Autorizaciones.Api
{
    public class ApiPerfilAutorizacion : ApiAutorizacionBase<Perfil>, IApiPerfilAutorizacion
    {
        public ApiPerfilAutorizacion(IVariablesSesion sesion) : base(sesion)
        {
        }

        public override ActionResult DenegarRemover(Perfil perfil)
        {
            var denegado = base.DenegarRemover(perfil);
            if (denegado != null)
                return denegado;

            if (perfil.Id == (int)Perfiles.Administrador)
                return HttpForbidden();

            return null;
        }

        public override ActionResult DenegarRemoverPorId(int? id)
        {
            if (id == (int)Perfiles.Administrador)
                return HttpForbidden();

            return base.DenegarRemoverPorId(id);
        }
    }
}