﻿using MiCons.Web.Providers;
using System.Linq;
using System.Web.Mvc;

namespace MiCons.Web
{
    public class ValueProviders
    {
        public static void RegisterFactories(ValueProviderFactoryCollection factories)
        {
            factories.Remove(factories.OfType<JsonValueProviderFactory>().FirstOrDefault());
            factories.Add(new JsonNetValueProviderFactory());
            factories.Add(new JsonNetUrlValueProviderFactory());

            var queryString = factories.OfType<QueryStringValueProviderFactory>().FirstOrDefault();
            factories.Remove(queryString);
            factories.Add(new NonJsonValueProviderFactory(queryString));
        }
    }
}