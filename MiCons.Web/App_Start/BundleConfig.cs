﻿using MiCons.Web.Dependencies.Modules;
using MiCons.Web.Helpers;
using System.Web.Optimization;

namespace MiCons.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/persona").Include<PersonaModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/home").Include<HomeModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/activacion").Include<ActivacionUsuarioModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/perfil").Include<PerfilModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/conjunto").Include<ConjuntoModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/novedad").Include<NovedadModuleDependencia>());

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/libs/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/libs/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/libs/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/libs/bootstrap.js",
                      "~/Scripts/libs/respond.js"));

            bundles.Add(new StyleBundle("~/Content/tabla").Include("~/Content/ng-table.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}