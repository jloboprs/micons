﻿using MiCons.Data.Entities;
using MiCons.Web.Dependencies;
using MiCons.Web.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MiCons.Web
{
    public class JsonConfig
    {
        static Formatting Formatting
        {
            get
            {
#if DEBUG
                return Formatting.Indented;
#else
                return Formatting.None;
#endif
            }
        }

        public static void ConfigurarJson()
        {
            JsonConvert.DefaultSettings = GenerarConfiguracionJson;
        }

        static JsonSerializerSettings GenerarConfiguracionJson()
        {
            var settings = new JsonSerializerSettings();

            settings.Formatting = Formatting;
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = ObtenerResolver();

            return settings;
        }

        private static IContractResolver ObtenerResolver()
        {
            var resolver = new SeleccionContractResolver();

            resolver.Quitar<DependenciaBase>().Propiedad(p => p.RutaArchivo);
            resolver.Quitar<Usuario>().Propiedad(p => p.Clave);

            return resolver;
        }
    }
}
