﻿using Autofac;
using Autofac.Integration.Mvc;
using MiCons.Data;
using System.Web.Mvc;

namespace MiCons.Web
{
    public class DependencyRegistration
    {
        public static void RegisterDependency()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<DataModuloDI>();
            builder.RegisterModule<WebModuloDI>();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}
