﻿using MiCons.Web.Providers;
using System.Web.Mvc;
using System.Web.Routing;

namespace MiCons.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes(new RoutePrefixInheritedProvider());

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "MiCons.Web.Controllers" }
            );
        }
    }
}
