﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Controllers
{
    public class HomeController : Controller
    {
        IRepoBase<TipoDocumento> _repo;
        public HomeController(IRepoBase<TipoDocumento> repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            if (ContextValue.EstaAutenticado)
                return Redirect("~/Novedad");

            return View(_repo.Obtener());
        }
    }
}