﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using MiCons.Web.Notificaciones;
using System.Web.Mvc;
using System.Web.Security;

namespace MiCons.Web.Controllers.Api
{
    public class UsuarioController : ApiControllerBase<Usuario>
    {
        INotificador _notificador;
        IPersonaRepo _personaRepo;
        IGeneradorHash _generador;

        protected new IUsuarioRepo Repo { get; set; }

        protected new IApiUsuarioAutorizacion Autorizacion { get; set; }

        public UsuarioController(IUsuarioRepo repo, IVariablesSesion sesion, IApiUsuarioAutorizacion autorizacion, INotificador notificador, IPersonaRepo personaRepo, IGeneradorHash generador) : base(repo, sesion, autorizacion)
        {
            Repo = repo;
            _notificador = notificador;
            _personaRepo = personaRepo;
            _generador = generador;
            Autorizacion = autorizacion;
        }

        public virtual ActionResult ObtenerEmpleados(ModeloConsulta solicitud)
        {
            return Json(Repo.ObtenerConPersona(solicitud));
        }

        [HttpGet]
        public ActionResult ObtenerPorEmail(string email)
        {
            var usuario = Repo.ObtenerPorEmail(email);
            if (usuario == null)
                return HttpNotFound();

            return Json(usuario);
        }

        public override ActionResult Modificar(Usuario usuario)
        {
            var denegado = Autorizacion.DenegarModificar(usuario);
            if (denegado != null)
                return denegado;

            if (!string.IsNullOrWhiteSpace(usuario.Clave))
            {
                var persona = usuario.Persona ?? _personaRepo.ObtenerPorId(usuario.IdPersona);
                usuario.Clave = _generador.GenerarHash(usuario, persona);
            }

            var usuarioModificado = Repo.Modificar(usuario);
            if (!usuarioModificado.Activo)
                _notificador.Notificar(usuarioModificado);

            return Json(usuarioModificado);
        }

        //TODO: Si el usuario autenticado no es administrador, no permitir colocar otros tipos de perfil
        public override ActionResult Agregar(Usuario usuario)
        {
            var denegado = Autorizacion.DenegarAgregar(usuario);
            if (denegado != null)
                return denegado;

            var persona = usuario.Persona ?? _personaRepo.ObtenerPorId(usuario.IdPersona);
            usuario.Clave = _generador.GenerarHash(usuario, persona);

            Repo.Agregar(usuario);
            _notificador.Notificar(usuario);

            return Json(usuario);
        }

        [HttpHead]
        public ActionResult Autenticar(Usuario usuario, bool persistente = false)
        {
            var denegado = Autorizacion.DenegarAutenticar(usuario, persistente);
            if (denegado != null)
                return denegado;

            var usuarioDb = Repo.ObtenerPorEmailConPersona(usuario.Email);
            if (usuarioDb == null || !usuarioDb.Activo)
                return HttpNotFound();

            if (!_generador.Comparar(usuario, usuarioDb.Persona, usuarioDb.Clave))
                return HttpNotFound();

            FormsAuthentication.SetAuthCookie(usuario.Email, persistente);
            return HttpNoContent();
        }

        [HttpPut]
        public ActionResult CambiarClave(Usuario usuario)
        {
            var denegado = Autorizacion.DenegarCambiarClave(usuario);
            if (denegado != null)
                return denegado;

            var persona = _personaRepo.ObtenerPorUsuarioId(usuario.Id);
            usuario.Clave = _generador.GenerarHash(usuario, persona);

            usuario = Repo.CambiarClave(usuario);

            return usuario != null ? Json(usuario) : HttpBadRequest();
        }

        [HttpHead]
        public ActionResult Desconectar()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return this.HttpNoContent();
        }
    }
}