﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;

namespace MiCons.Web.Controllers.Api
{
    public class ResidenciaController : ApiControllerBase<Residencia>
    {
        public ResidenciaController(IRepoBase<Residencia> repo, IVariablesSesion sesion, IApiAutorizacionBase<Residencia> autorizacion) : base(repo, sesion, autorizacion)
        {
        }
    }
}