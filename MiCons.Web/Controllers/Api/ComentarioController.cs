﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using System;
using System.Web.Mvc;

namespace MiCons.Web.Controllers.Api
{
    public class ComentarioController : ApiControllerBase<Comentario>
    {
        private IPersonaRepo _personaRepo;
        protected new IComentarioRepo Repo { get; set; }

        public ComentarioController(IComentarioRepo repo, IVariablesSesion sesion, IApiAutorizacionBase<Comentario> autorizacion, IPersonaRepo personaRepo) : base(repo, sesion, autorizacion)
        {
            Repo = repo;
            _personaRepo = personaRepo;
        }

        public ActionResult ObtenerPorNovedad(int idNovedad)
        {
            return Json(Repo.ObtenerPorNovedad(idNovedad));
        }

        public override ActionResult Agregar(Comentario comentario)
        {
            comentario.Fecha = DateTime.Now;
            comentario.IdPersona = Sesion.Usuario.IdPersona;
            comentario.Persona = _personaRepo.ObtenerPorId(comentario.IdPersona);

            return base.Agregar(comentario);
        }
    }
}