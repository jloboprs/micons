﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using MiCons.Data.Repositories;
using MiCons.Web.Attributes;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace MiCons.Web.Controllers.Api
{
    [RoutePrefixInherited("api/{controller}/{action=index}/{id?}")]
    public abstract class ApiControllerBase<T> : Controller where T : class, IEntity
    {
        protected IRepoBase<T> Repo { private set; get; }
        protected IApiAutorizacionBase<T> Autorizacion { private set; get; }
        protected IVariablesSesion Sesion { private set; get; }

        public ApiControllerBase(IRepoBase<T> repo, IVariablesSesion sesion, IApiAutorizacionBase<T> autorizacion)
        {
            Repo = repo;
            Autorizacion = autorizacion;
            Sesion = sesion;
        }

        public virtual ActionResult Obtener(ModeloConsulta solicitud)
        {
            var denegado = Autorizacion.DenegarObtener(solicitud);
            if (denegado != null)
                return denegado;

            return Json(Repo.Obtener(solicitud), JsonRequestBehavior.AllowGet);
        }

        [ActionName("index")]
        public virtual ActionResult ObtenerPorId(int? id)
        {
            var denegado = Autorizacion.DenegarObtenerPorId(id);
            if (denegado != null)
                return denegado;

            var data = Repo.ObtenerPorId(id.Value);
            if (data == null)
                return HttpNotFound();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("index")]
        public virtual ActionResult Agregar(T data)
        {
            var denegado = Autorizacion.DenegarAgregar(data);
            if (denegado != null)
                return denegado;

            return Json(Repo.Agregar(data));
        }

        [HttpPut, ActionName("index")]
        public virtual ActionResult Modificar(T data)
        {
            var denegado = Autorizacion.DenegarModificar(data);
            if (denegado != null)
                return denegado;

            var newData = Repo.Modificar(data);
            if (newData == null)
                return HttpNotFound();

            return Json(newData);
        }

        [HttpDelete]
        public virtual ActionResult Remover(T data)
        {
            var denegado = Autorizacion.DenegarRemover(data);
            if (denegado != null)
                return denegado;

            return Repo.Remover(data) ? Json(data) : HttpNotFound() as ActionResult;
        }

        [HttpDelete, ActionName("index")]
        public virtual ActionResult RemoverPorId(int? id)
        {
            if (!id.HasValue)
                return HttpBadRequest();

            var denegado = Autorizacion.DenegarRemoverPorId(id);
            if (denegado != null)
                return denegado;

            return Repo.Remover(id.Value) ? HttpNoContent() : HttpNotFound();
        }

        protected ActionResult HttpBadRequest()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        protected ActionResult HttpNoContent()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }

        protected ActionResult HttpNotModified()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotModified);
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult(data) { ContentType = contentType, ContentEncoding = contentEncoding };
        }
    }
}