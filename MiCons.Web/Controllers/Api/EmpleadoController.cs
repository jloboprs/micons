﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;

namespace MiCons.Web.Controllers.Api
{
    public class EmpleadoController : ApiControllerBase<Empleado>
    {
        public EmpleadoController(IRepoBase<Empleado> repo, IVariablesSesion sesion, IApiAutorizacionBase<Empleado> autorizacion) : base(repo, sesion, autorizacion)
        {
        }
    }
}