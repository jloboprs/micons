﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Controllers.Api
{
    public class PersonaController : ApiControllerBase<Persona>
    {
        protected new IPersonaRepo Repo { get; set; }
        protected new IApiPersonaAutorizacion Autorizacion { get; set; }

        public PersonaController(IPersonaRepo repo, IVariablesSesion sesion, IApiPersonaAutorizacion autorizacion) : base(repo, sesion, autorizacion)
        {
            Repo = repo;
            Autorizacion = autorizacion;
        }

        [HttpGet]
        public ActionResult ObtenerPorDocumento(int idTipoDocumento, string numeroDocumento)
        {
            var persona = Repo.ObtenerPorDocumento(idTipoDocumento, numeroDocumento);
            if (persona == null)
                return HttpNotFound();

            return Json(persona);
        }

        [HttpPut]
        public ActionResult Activar(Persona persona)
        {
            var denegado = Autorizacion.DenegarActivar(persona);
            if (denegado != null)
                return denegado;

            persona = Repo.Activar(persona);
            if (persona == null)
                return HttpBadRequest();

            return Json(persona);
        }
    }
}