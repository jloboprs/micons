﻿using MiCons.Data.Entities;
using MiCons.Data.Models;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;

namespace MiCons.Web.Controllers.Api
{
    public class NovedadController : ApiControllerBase<Novedad>
    {
        protected new INovedadRepo Repo { get; set; }

        public NovedadController(INovedadRepo repo, IVariablesSesion sesion, IApiNovedadAutorizacion autorizacion) : base(repo, sesion, autorizacion)
        {
            Repo = repo;
        }

        public override ActionResult Obtener(ModeloConsulta solicitud)
        {
            var denegado = Autorizacion.DenegarObtener(solicitud);
            if (denegado != null)
                return denegado;

            if (Sesion.Usuario.EsAdministrador)
                return Json(Repo.ObtenerPorAdministrador(Sesion.Usuario.IdPersona, solicitud));

            if (Sesion.Usuario.EsEmpleado)
                return Json(Repo.ObtenerPorEmpleado(Sesion.Usuario.IdPersona, solicitud));

            return Json(Repo.ObtenerPorResidente(Sesion.Usuario.IdPersona, solicitud));
        }

        public override ActionResult Modificar(Novedad novedad)
        {
            var denegado = Autorizacion.DenegarModificar(novedad);
            if (denegado != null)
                return denegado;

            var comentarios = novedad.Comentarios ?? (novedad.Comentarios = new List<Comentario>());
            comentarios.Add(AsignarValoresPorDefecto(new Comentario
            {
                IdNovedad = novedad.Id,
                Descripcion = "El usuario ha modificado la novedad"
            }));

            try
            {
                return base.Modificar(novedad);
            }
            catch (DbUpdateConcurrencyException e)
            {
                return HttpNotModified();
            }
        }

        public override ActionResult Agregar(Novedad novedad)
        {
            novedad.Fecha = DateTime.Now;
            novedad.IdPersonaCreada = Sesion.Usuario.IdPersona;
            novedad.PersonaCreada = null;

            if (novedad.Comentarios != null)
                foreach (var comentario in novedad.Comentarios)
                    AsignarValoresPorDefecto(comentario);

            return base.Agregar(novedad);
        }

        private Comentario AsignarValoresPorDefecto(Comentario comentario)
        {
            comentario.Fecha = DateTime.Now;
            comentario.IdPersona = Sesion.Usuario.IdPersona;

            return comentario;
        }
    }
}