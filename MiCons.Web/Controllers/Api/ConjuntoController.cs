﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Controllers.Api
{
    public class ConjuntoController : ApiControllerBase<Conjunto>
    {
        protected new IConjuntoRepo Repo { get; set; }

        public ConjuntoController(IConjuntoRepo repo, IVariablesSesion sesion, IApiAutorizacionBase<Conjunto> autorizacion) : base(repo, sesion, autorizacion)
        {
            Repo = repo;
        }

        public ActionResult ObtenerPorPersona(int idPersona)
        {
            return Json(Repo.ObtenerPorPersona(idPersona));
        }
    }
}