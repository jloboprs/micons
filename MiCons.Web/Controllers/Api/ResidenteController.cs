﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;

namespace MiCons.Web.Controllers.Api
{
    public class ResidenteController : ApiControllerBase<Residente>
    {
        public ResidenteController(IResidenteRepo repo, IVariablesSesion sesion, IApiAutorizacionBase<Residente> autorizacion) : base(repo, sesion, autorizacion)
        {
        }
    }
}