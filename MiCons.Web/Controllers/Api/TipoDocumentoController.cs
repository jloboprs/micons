﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;

namespace MiCons.Web.Controllers.Api
{
    public class TipoDocumentoController : ApiControllerBase<TipoDocumento>
    {
        public TipoDocumentoController(IRepoBase<TipoDocumento> repo, IVariablesSesion sesion, IApiAutorizacionBase<TipoDocumento> autorizacion) : base(repo, sesion, autorizacion)
        {
        }
   }
}