﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Autorizaciones.Api;
using MiCons.Web.Helpers;

namespace MiCons.Web.Controllers.Api
{
    public class PerfilController : ApiControllerBase<Perfil>
    {
        public PerfilController(IPerfilRepo repo, IVariablesSesion sesion, IApiPerfilAutorizacion autorizacion) : base(repo, sesion, autorizacion)
        {
        }
    }
}