﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Helpers;
using System.Web.Mvc;

namespace MiCons.Web.Controllers
{
    public class PerfilController : Controller
    {
        IRepoBase<Perfil> _repo;
        IVariablesSesion _sesion;

        public PerfilController(IRepoBase<Perfil> repo, IVariablesSesion sesion)
        {
            _repo = repo;
            _sesion = sesion;
        }

        public ActionResult Index()
        {
            if (!_sesion.Usuario.EsAdministrador)
                return new HttpNotFoundResult();

            return View(_repo.Obtener());
        }
    }
}