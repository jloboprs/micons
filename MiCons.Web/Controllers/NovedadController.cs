﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Models;
using System.Web.Mvc;

namespace MiCons.Web.Controllers
{
    public class NovedadController : Controller
    {
        IRepoBase<Estado> _estadoRepo;
        IPerfilRepo _perfilRepo;
        IPersonaRepo _personaRepo;

        public NovedadController(IRepoBase<Estado> estadoRepo, IPerfilRepo perfilRepo, IPersonaRepo personaRepo)
        {
            _estadoRepo = estadoRepo;
            _perfilRepo = perfilRepo;
            _personaRepo = personaRepo;
        }

        public ActionResult Index()
        {
            return View(new NovedadModel
            {
                Estados = _estadoRepo.Obtener(),
                Perfiles = _perfilRepo.Obtener(),
                Personas = _personaRepo.ObtenerEmpleados()
            });
        }
    }
}