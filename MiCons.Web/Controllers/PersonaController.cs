﻿using MiCons.Data.Entities;
using MiCons.Data.Repositories;
using MiCons.Web.Helpers;
using MiCons.Web.Models;
using System.Web.Mvc;

namespace MiCons.Web.Controllers
{
    public class PersonaController : Controller
    {
        IUsuarioRepo _repoUsuario;
        IRepoBase<TipoDocumento> _repoTipoDocumento;
        IRepoBase<Conjunto> _repoConjunto;
        IRepoBase<Perfil> _repoPerfil;
        IVariablesSesion _sesion;

        public PersonaController(IUsuarioRepo repoUsuario, IRepoBase<TipoDocumento> repoTipoDocumento, IRepoBase<Conjunto> repoConjunto, IRepoBase<Perfil> repoPerfil, IVariablesSesion sesion)
        {
            _repoUsuario = repoUsuario;
            _repoTipoDocumento = repoTipoDocumento;
            _repoConjunto = repoConjunto;
            _repoPerfil = repoPerfil;
            _sesion = sesion;
        }

        public ActionResult Index()
        {
            if (!_sesion.Usuario.EsAdministrador)
                return new HttpNotFoundResult();

            var persona = new PersonaCompletaModel
            {
                Usuarios = _repoUsuario.ObtenerConPersona(),
                TiposDocumentos = _repoTipoDocumento.Obtener(),
                Conjuntos = _repoConjunto.Obtener(),
                Perfiles = _repoPerfil.Obtener()
            };

            return View(persona);
        }
    }
}