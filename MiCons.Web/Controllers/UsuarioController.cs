﻿using MiCons.Data.Repositories;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace MiCons.Web.Controllers
{
    [RoutePrefix("usuario")]
    public class UsuarioController : Controller
    {
        IUsuarioRepo _repo;
        public UsuarioController(IUsuarioRepo repo)
        {
            _repo = repo;
        }

        [Route("Activar/{codigo}/{id}/{reiniciarClave}")]
        public ActionResult Activar(int id, Guid codigo, bool reiniciarClave)
        {
            var usuario = _repo.Activar(id, codigo);
            if (usuario == null)
                return HttpNotFound();

            if (!reiniciarClave)
                return RedirectToAction("Index", "Home", new { msg = "El usuario fue activado exitosamente. Autentíquese para utilizar la aplicación" });

            return View(usuario);
        }

        public ActionResult Desconectar()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Index", "Home", new { msg = "La sesión fue cerrada" });
        }
    }
}