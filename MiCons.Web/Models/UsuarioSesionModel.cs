﻿namespace MiCons.Web.Models
{
    public class UsuarioSesionModel
    {
        public string Email { get; set; }

        public string Nombre { get; set; }

        public bool EsResidente { get; set; }

        public bool EsEmpleado { get; set; }

        public bool EsAdministrador { get; set; }

        public int IdPersona { get; set; }

        public int IdTipoDocumento { get; set; }

        public string NumeroDocumento { get; set; }

        public bool Activo { get; set; }
    }
}