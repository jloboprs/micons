﻿using System.Collections.Generic;
using MiCons.Data.Entities;

namespace MiCons.Web.Models
{
    public class NovedadModel
    {
        public IEnumerable<Estado> Estados { get; set; }
        public IEnumerable<Perfil> Perfiles { get; set; }
        public IEnumerable<Persona> Personas { get; set; }
    }
}