﻿using MiCons.Data.Entities;
using System.Collections.Generic;

namespace MiCons.Web.Models
{
    public class PersonaCompletaModel
    {
        public IEnumerable<Usuario> Usuarios { get; set; }
        public IEnumerable<TipoDocumento> TiposDocumentos { get; set; }
        public IEnumerable<Conjunto> Conjuntos { get; set; }
        public IEnumerable<Perfil> Perfiles { get; set; }
    }
}