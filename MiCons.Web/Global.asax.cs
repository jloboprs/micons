﻿using MiCons.Web.Providers;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MiCons.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            ValueProviders.RegisterFactories(ValueProviderFactories.Factories);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyRegistration.RegisterDependency();
            JsonConfig.ConfigurarJson();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs args)
        {
            AutenticacionRequestProvider.AsignarAutenticacion();
        }
    }
}