var PersonaModule = function PersonaModule(dato) {
    angular.module('app', ['ngCookies', 'ngTable', 'xtForm'])
        .directive('ngEmailUnico', NgEmailUnicoValidation)
        .directive('ngDocumentoUnico', NgDocumentoUnicoValidation)
        .config(XtFormConfig)
        .config(HttpConfig)
        .service('defaultInterceptor', DefaultInterceptor)
        .service('personaApi', PersonaApi)

        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .controller('autenticacionController', AutenticacionController)
        .directive('ngRemoveClass', NgRemoveClassDirective)

        .value('usuariosValue', dato.Usuarios)
        .value('tiposDocumentosValue', dato.TiposDocumentos)
        .value('perfilesValue', dato.Perfiles)
        .value('conjuntosValue', dato.Conjuntos)
        .service('usuarioApi', UsuarioApi)
        .service('residenteApi', ResidenteApi)
        .service('residenciaApi', ResidenciaApi)
        .service('empleadoApi', EmpleadoApi)
        .controller('personaController', PersonaController);
};