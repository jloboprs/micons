var HomeModule = function HomeModule(tiposDocumentos) {
    angular.module('app', ['xtForm', 'ngCookies'])
        .value('tiposDocumentosValue', tiposDocumentos)
        .directive('ngRemoveClass', NgRemoveClassDirective)
        .directive('ngEmailUnico', NgEmailUnicoValidation)
        .directive('ngDocumentoUnico', NgDocumentoUnicoValidation)
        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .service('personaApi', PersonaApi)
        .service('defaultInterceptor', DefaultInterceptor)
        .controller('usuarioController', UsuarioController)
        .controller('autenticacionController', AutenticacionController)
        .config(XtFormConfig)
        .config(HttpConfig);
};