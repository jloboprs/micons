var ConjuntoModule = function ConjuntoModule(conjuntos) {
    angular.module('app', ['ngCookies', 'ngTable'])
        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .controller('autenticacionController', AutenticacionController)
        .directive('ngRemoveClass', NgRemoveClassDirective)

        .value('conjuntosValue', conjuntos)
        .service('conjuntoApi', ConjuntoApi)
        .service('residenciaApi', ResidenciaApi)
        .controller('conjuntoController', ConjuntoController);
};