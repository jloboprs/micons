var PerfilModule = function PerfilModule(perfiles) {
    angular.module('app', ['ngCookies', 'ngTable'])
        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .controller('autenticacionController', AutenticacionController)
        .directive('ngRemoveClass', NgRemoveClassDirective)

        .value('perfilesValue', perfiles)
        .service('perfilApi', PerfilApi)
        .controller('perfilController', PerfilController);
};