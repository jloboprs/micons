var NovedadModule = function NovedadModule(dato) {
    angular.module('app', ['ngCookies', 'xtForm'])
        .config(HttpConfig)
        .service('defaultInterceptor', DefaultInterceptor)
        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .directive('ngRemoveClass', NgRemoveClassDirective)
        .controller('autenticacionController', AutenticacionController)

        .config(XtFormConfig)        
        .value('estadosValue', dato.Estados)
        .value('perfilesValue', dato.Perfiles)
        .value('personasValue', dato.Personas)
        .service('novedadApi', NovedadApi)
        .service('conjuntoApi', ConjuntoApi)
        .service('residenciaApi', ResidenciaApi)
        .service('comentarioApi', ComentarioApi)
        .service('sesionService', SesionService)
        .controller('novedadController', NovedadController);
};