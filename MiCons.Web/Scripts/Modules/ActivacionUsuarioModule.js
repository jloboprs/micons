var ActivacionUsuarioModule = function ActivacionUsuarioModule(usuario) {
    angular.module('app', ['ngCookies', 'xtForm'])
        .config(HttpConfig)
        .service('defaultInterceptor', DefaultInterceptor)
        .directive('ngRemoveClass', NgRemoveClassDirective)
        
        .service('sesionService', SesionService)
        .service('usuarioApi', UsuarioApi)
        .controller('autenticacionController', AutenticacionController)

        .config(XtFormConfig)
        .directive('ngEquals', NgEqualsValidation)

        .controller('activacionUsuarioController', ActivacionUsuarioController)
        .value('usuarioValue', usuario);
};