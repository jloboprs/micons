var NgRemoveClassDirective = function NgRemoveClassDirective($animate) {
    return {
        restrict: 'A',
        link: function link(scope, element, attrs, controllers) {
            $animate.removeClass(element, attrs.ngRemoveClass)
        }
    };
}