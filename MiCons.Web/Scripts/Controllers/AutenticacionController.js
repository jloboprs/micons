var AutenticacionController = function AutenticacionController(usuarioApi, sesionService) {
    this.usuarioApi = usuarioApi;
    this.sesionService = sesionService;
};

AutenticacionController.prototype.autenticar = function autenticar() {
    this.usuarioApi.autenticar(this.usuario, true).then(this.__mostrarAutenticacion);
};

AutenticacionController.prototype.__mostrarAutenticacion = function __mostrarAutenticacion(exitosa) {
    if (!exitosa)
        return alert.danger('La autenticación fue fallida');

    window.location = '/Novedad';
};

AutenticacionController.prototype.estaAutenticado = function estaAutenticado() {
    return this.sesionService.estaAutenticado();
};

AutenticacionController.prototype.esAdministrador = function esAdministrador() {
    return this.sesionService.estaAutenticado() && this.sesionService.sesion.EsAdministrador;
};

AutenticacionController.prototype.obtenerSesion = function obtenerSesion() {
    return this.sesionService.sesion;
};