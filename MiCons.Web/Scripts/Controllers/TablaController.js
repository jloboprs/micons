var TablaController = function TablaController(api, datos, NgTableParams) {
    this.api = api;
    this.datos = datos;
    this.datosTabla = angular.copy(datos);
    this.onAgregar = undefined;
    this.onGuardar = undefined;
    this.onAgregardo = undefined;

    this.tablaConfig = new NgTableParams(
        { sorting: { Id: "asc" } },
        {   
            filterDelay: 0,
            dataset: this.datosTabla,
            counts: []
        });
};

TablaController.prototype.asignarDatos = function asignarDatos(datos) {
    this.datos = datos;
    this.datosTabla = angular.copy(datos);
    this.tablaConfig.settings({ dataset: this.datosTabla });
};

TablaController.prototype.agregarElemento = function agregarElemento() {
    var nuevo = { enEdicion: true };
    
    if (this.onAgregar)
        this.onAgregar(nuevo);

    this.datosTabla.unshift(nuevo);
    this.tablaConfig.reload();
};

TablaController.prototype.editarElemento = function editarElemento(registro) {
    registro.enEdicion = true;
};

TablaController.prototype.cancelarElemento = function cancelarElemento(registro, edicionForm) {
    if (!registro.Id)
        return this.__removerElementoDeTabla(registro);

    this.__reiniciarEdicion(registro, edicionForm);
    angular.extend(registro, this.obtenerRegistroOriginal(registro));
};

TablaController.prototype.removerElemento = function removerElemento(registro) {
    this.api.remover(registro).then(this.__removerRespuesta.bind(this, registro));
};

TablaController.prototype.guardarElemento = function guardarElemento(registro, edicionForm) {
    if (this.onGuardar)
        this.onGuardar(registro, this.obtenerRegistroOriginal(registro));

    if (registro.Id)
        this.api.modificar(registro).then(this.__modificarRespuesta.bind(this, edicionForm, registro));
    else
        this.api.agregar(registro).then(this.__agregarRespuesta.bind(this, edicionForm, registro));
};

TablaController.prototype.puedeGuardar = function puedeGuardar(edicionForm) {
    return edicionForm.$dirty && edicionForm.$valid;
};

TablaController.prototype.__removerRespuesta = function __removerRespuesta(registro) {
    this.__removerElementoDeTabla(registro);
}

TablaController.prototype.__modificarRespuesta = function __modificarRespuesta(edicionForm, registro) {
    this.__reiniciarEdicion(registro, edicionForm);
    angular.extend(this.obtenerRegistroOriginal(registro), registro);
}

TablaController.prototype.__agregarRespuesta = function __agregarRespuesta(edicionForm, registro, nuevoRegistro) {
    this.__reiniciarEdicion(registro, edicionForm);
    registro.Id = nuevoRegistro.Id;
    if (this.onAgregardo)
        this.onAgregardo(registro, nuevoRegistro)

    this.datos.push(registro);
    this.tablaConfig.reload();
}

TablaController.prototype.__removerElementoDeTabla = function __removerElementoDeTabla(registro) {
    _.remove(this.datosTabla, function(item) {
        return registro === item;
    });

    this.tablaConfig.reload();
};

TablaController.prototype.__reiniciarEdicion = function __reiniciarEdicion(registro, edicionForm){
    delete registro.enEdicion;
    edicionForm.$setPristine();
};

TablaController.prototype.obtenerRegistroOriginal = function obtenerRegistroOriginal(registro){
    return _.find(this.datos, function(registroOriginal){
        return registroOriginal.Id === registro.Id;
    });
};