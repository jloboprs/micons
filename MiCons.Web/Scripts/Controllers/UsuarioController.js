var UsuarioController = function UsuarioController(tiposDocumentosValue, usuarioApi, $scope) {
    this.tiposDocumentos = tiposDocumentosValue;
    this.usuarioApi = usuarioApi;
    this.$scope = $scope;
    
    this.validarEmail = this.validarEmail.bind(this);
    this.validarDocumento = this.validarDocumento.bind(this);
    this.__mostrarCreacionUsuarioExitosa = this.__mostrarCreacionUsuarioExitosa.bind(this);
    this.__reiniciarFormulario();
    this.mostrarMensaje();
};

UsuarioController.prototype.crearUsuario = function crearUsuario() {
    var solicitud = !this.nuevo.Id 
        ? this.usuarioApi.agregar(this.nuevo)
        : this.usuarioApi.modificar(this.nuevo);
    
    solicitud.then(this.__mostrarCreacionUsuarioExitosa);
};

UsuarioController.prototype.autenticar = function autenticar() {
    this.usuarioApi.autenticar(this.credencial, true).then(this.__mostrarAutenticacion);
};

UsuarioController.prototype.validarEmail = function validarEmail(usuario) {
    if (usuario === undefined) {
        this.nuevo.Id = undefined;
        return true;
    }

    this.nuevo.Id = usuario.Id;
    return this.emailUnico  = !usuario.Activo;
};

UsuarioController.prototype.validarDocumento = function validarDocumento(persona) {
    if (persona === undefined) {
        this.nuevo.IdPersona = undefined;
        this.nuevo.Persona.Id = undefined;
        return true;
    }

    this.nuevo.IdPersona = persona.Id;
    this.nuevo.Persona.Id = persona.Id;
    return !persona.Activo;
};

UsuarioController.prototype.__mostrarCreacionUsuarioExitosa = function __mostrarCreacionUsuarioExitosa(usuario) {
    this.__reiniciarFormulario();
    alert.success('El usuario se ha creado con éxito. Le hemos enviado un correo para verificar la cuenta');
};

UsuarioController.prototype.__mostrarAutenticacion = function __mostrarAutenticacion(exitosa) {
    if (!exitosa)
        return alert.danger('La autenticación fue fallida');

    window.location = '/tablero';
};

UsuarioController.prototype.__reiniciarFormulario = function __reiniciarFormulario() {
    this.nuevo = { Persona: { IdTipoDocumento:  this.tiposDocumentos[0].Id } };
    
    if (!this.$scope.frmUsuario)
        return;
    
    this.$scope.frmUsuario.$setUntouched();
    this.$scope.frmUsuario.$setPristine();
};

UsuarioController.prototype.mostrarMensaje = function mostrarMensaje() {
    var msg = this.__obtenerParametroPorNombre('msg');
    if (msg !== undefined && msg !== null && msg !== '')
        alert(this.__obtenerParametroPorNombre('msg'));
};

UsuarioController.prototype.__obtenerParametroPorNombre = function __obtenerParametroPorNombre(nombre, url) {
    if (!url) url = window.location.href;
    nombre = nombre.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + nombre + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};