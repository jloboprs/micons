var ActivacionUsuarioController = function ActivacionUsuarioController(usuarioValue, usuarioApi) {
    this.usuario = usuarioValue;
    this.usuarioApi = usuarioApi;
};

ActivacionUsuarioController.prototype.cambiarClave = function cambiarClave() {
    this.usuario.ReiniciarClave = false;
    this.usuarioApi.cambiarClave(this.usuario).then(this.__respuestaCambiarClave);
};

ActivacionUsuarioController.prototype.__respuestaCambiarClave = function __respuestaCambiarClave() {
    return location.href = "/";
};