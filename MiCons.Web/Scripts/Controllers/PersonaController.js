var PersonaController = function PersonaController(usuarioApi, personaApi, empleadoApi, residenteApi, residenciaApi, usuariosValue,
    perfilesValue, conjuntosValue, tiposDocumentosValue, NgTableParams, $q) {
    this.$q = $q;
    this.personaApi = personaApi;
    this.empleadoApi = empleadoApi;
    this.residenteApi = residenteApi;
    this.residenciaApi = residenciaApi;
    this.perfiles = perfilesValue;
    this.conjuntos = conjuntosValue;
    this.tiposDocumentos = tiposDocumentosValue;
    this.usuarios = new TablaController(usuarioApi, usuariosValue, NgTableParams);
    this.empleados = new TablaController(empleadoApi, [], NgTableParams);
    this.residentes = new TablaController(residenteApi, [], NgTableParams);
    
    this.usuarios.onAgregar = this.onAgregarUsuario.bind(this);
    this.usuarios.onGuardar = this.onGuardarUsuario;
    this.usuarios.onAgregardo = this.onUsuarioAgregardo;
    this.empleados.onGuardar = this.onGuardarEmpleado.bind(this);
    this.residentes.onGuardar = this.onGuardarResidente.bind(this);
    this.residentes.editarElemento = this.onEditarResidente.bind(this);

    this.esPersona = true;
    this.residencias = [];
    this.personaSeleccionada = undefined;
};

PersonaController.prototype.seleccionarPersona = function seleccionarPersona(usuario) {
    this.personaSeleccionada = usuario;
    var consulta = {
        Filters: [{
            Field: 'IdPersona',
            Value: usuario.IdPersona,
            Operator: 'eq'
        }]
    };
    
    this.$q.all([this.empleadoApi.obtener(consulta), this.residenteApi.obtener(consulta)]).then(function(respuesta) {
        this.empleados.asignarDatos(respuesta[0]);
        this.residentes.asignarDatos(respuesta[1]);
        this.esPersona = false;
    }.bind(this))
};

PersonaController.prototype.obtenerResidencias = function obtenerResidencias(residente) {
    var consulta = {
        Filters: [{
            Field: 'IdConjunto',
            Value: residente.Residencia.IdConjunto,
            Operator: 'eq'
        }]
    };
    
    return this.residenciaApi.obtener(consulta).then(function(respuesta) {
        this.residencias = respuesta;
    }.bind(this))
};

PersonaController.prototype.activarPersona = function activarPersona(usuario) { 
    this.personaApi.activar({Id: usuario.Persona.Id, Activo: !usuario.Persona.Activo}).then(function(persona) {
        var usuarioOriginal = this.usuarios.obtenerRegistroOriginal(usuario);
        usuarioOriginal.Persona.Activo = persona.Activo;
        usuario.Persona.Activo = persona.Activo;
    }.bind(this))
};

PersonaController.prototype.onEditarResidente = function onEditarResidente(residente) { 
    this.obtenerResidencias(residente).then(function() {
        TablaController.prototype.editarElemento.call(this.residentes, residente);
    }.bind(this))
};

PersonaController.prototype.onAgregarUsuario = function onAgregarUsuario(usuario) { 
    usuario.Persona = { IdTipoDocumento: this.tiposDocumentos[0].Id, Activo: true };
};

PersonaController.prototype.onGuardarUsuario = function onGuardarUsuario(usuario, usuarioAnterior) { 
    if (usuarioAnterior === undefined || usuario.Email !== usuarioAnterior.Email) {
       usuario.Activo = false;
       usuario.ReiniciarClave = true;
    }

    var persona = usuario.Persona;
    var personaAnterior = usuarioAnterior ? usuarioAnterior.Persona : undefined;
    if (personaAnterior === undefined ||
        persona.NumeroDocumento !== personaAnterior.NumeroDocumento ||
        persona.IdTipoDocumento !== personaAnterior.IdTipoDocumento)
        persona.Activo = true
};

PersonaController.prototype.onUsuarioAgregardo = function onUsuarioAgregardo(usuario, usuarioNuevo) { 
    usuario.Persona.Id = usuarioNuevo.Persona.Id;
    usuario.IdPersona = usuarioNuevo.IdPersona;
};

PersonaController.prototype.onGuardarEmpleado = function onGuardarEmpleado(empleado) { 
    empleado.IdPersona = this.personaSeleccionada.IdPersona;
};

PersonaController.prototype.onGuardarResidente = function onGuardarResidente(residente) { 
    residente.IdPersona = this.personaSeleccionada.IdPersona;
};

PersonaController.prototype.ObtenerDescripcionPerfil = function ObtenerDescripcionPerfil(empleado) { 
    for (var i = this.perfiles.length - 1; i >= 0; i--) {
        if (this.perfiles[i].Id === empleado.IdPerfil)
            return this.perfiles[i].Descripcion;
    }
};

PersonaController.prototype.ObtenerDescripcionConjuntos = function ObtenerDescripcionConjuntos(idConjunto) { 
    for (var i = this.conjuntos.length - 1; i >= 0; i--) {
        if (this.conjuntos[i].Id === idConjunto)
            return this.conjuntos[i].Nombre;
    }
};

PersonaController.prototype.ObtenerDescripcionDocumento = function ObtenerDescripcionDocumento(usuario) { 
    for (var i = this.tiposDocumentos.length - 1; i >= 0; i--) {
        if (this.tiposDocumentos[i].Id === usuario.Persona.IdTipoDocumento)
            return this.tiposDocumentos[i].Codigo;
    }
};

PersonaController.prototype.AgregarResidencia = function AgregarResidencia(residente) { 
    for (var i = this.residencias.length - 1; i >= 0; i--)
        if (this.residencias[i].Id === residente.IdResidencia)
            return residente.Residencia = this.residencias[i];
};