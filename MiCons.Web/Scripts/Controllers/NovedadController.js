var NovedadController = function NovedadController(novedadApi, conjuntoApi, residenciaApi, comentarioApi, sesionService, estadosValue, perfilesValue, personasValue, $q) {
    this.$q = $q;
    this.novedadApi = novedadApi;
    this.conjuntoApi = conjuntoApi;
    this.residenciaApi = residenciaApi;
    this.comentarioApi = comentarioApi;
    this.sesionService = sesionService;
    this.estados = estadosValue;
    this.perfiles = perfilesValue;
    this.personas = personasValue;
    this.novedades = [];
    this.conjuntos = [];
    this.residencias = [];
    this.comentarios = [];
    this.novedadSeleccionada = undefined;
    this.comentario = { Descripcion: undefined };
    this.__consultaParametro = {
        OrderBy: undefined,
        IsPageable: true,
        Ascending: true,
        Take: 10
    };

    this.__consultarNovedades = this.__consultarNovedades.bind(this);
    this.__asignarConjuntos = this.__asignarConjuntos.bind(this);
    this.__consultarNovedades();
    this.__asignarConjuntos();
};

NovedadController.prototype.__consultarNovedades = function __consultarNovedades(novedades) {
    if (novedades)
        return this.novedades = novedades;
        
    this.novedadApi.obtener(this.__consultaParametro).then(this.__consultarNovedades);
};

NovedadController.prototype.__asignarConjuntos = function __asignarConjuntos(conjuntos) {
    if (conjuntos)
        return this.conjuntos = conjuntos;

    (this.sesionService.sesion.EsResidente
        ? this.conjuntoApi.obtenerPorPersona(this.sesionService.sesion.IdPersona)
        : this.conjuntoApi.obtener()).then(this.__asignarConjuntos);
};

NovedadController.prototype.obtenerResidencias = function obtenerResidencias(novedad) {
    if (this.sesionService.sesion.EsResidente) {
        for (var i = 0; i < this.conjuntos.length; i++) {
            if (this.conjuntos[i].Id !== novedad.IdConjunto)
                continue;
            
            return this.$q.resolve(this.residencias = this.conjuntos[i].Residencias);
        }
    }

    var consulta = {
        Filters: [{
            Field: 'IdConjunto',
            Value: novedad.IdConjunto,
            Operator: 'eq'
        }]
    };        
    
    return this.residenciaApi.obtener(consulta).then(function(respuesta) {
        return this.residencias = respuesta;
    }.bind(this));
};

NovedadController.prototype.ordenarPor = function ordenarPor(propiedad) {
    if (this.__consultaParametro.OrderBy !== propiedad)
        this.__consultaParametro.Ascending = true;
    else
        this.__consultaParametro.Ascending = !this.__consultaParametro.Ascending;

    this.__consultaParametro.OrderBy = propiedad;
    this.__consultarNovedades();
};

NovedadController.prototype.esAscendente = function esAscendente(propiedad) {
    return this.__consultaParametro.OrderBy === propiedad && this.__consultaParametro.Ascending;
};

NovedadController.prototype.esDescendente = function esDescendente(propiedad) {
    return this.__consultaParametro.OrderBy === propiedad && !this.__consultaParametro.Ascending;
};

NovedadController.prototype.obtenerComentarios = function obtenerComentarios(novedad) {
    return this.comentarioApi.obtenerPorNovedad(novedad.Id).then(function(comentarios) {
        return this.comentarios = comentarios;
    }.bind(this));
};

NovedadController.prototype.seleccionarNovedad = function seleccionarNovedad(novedad) {
    this.$q.all([this.obtenerComentarios(novedad), this.obtenerResidencias(novedad)]).then(function() {
        this.comentario.Descripcion = undefined;
        this.novedadSeleccionada = angular.extend(Object.create(novedad), novedad);
    }.bind(this));
};

NovedadController.prototype.agregarNovedad = function agregarNovedad() {
    this.comentario.Descripcion = undefined;
    this.comentarios = [];
    var novedad = {
        Fecha: new Date(),
        IdEstado: 1,
        IdConjunto: this.conjuntos[0].Id,
        IdPersonaCreada: this.sesionService.sesion.IdPersona,
        PersonaCreada: {
            Id: this.sesionService.sesion.IdPersona,
            Nombre: this.sesionService.sesion.Nombre
        }
    };

    this.obtenerResidencias(novedad).then(function(residencias) {
        if (residencias.length > 0)
            novedad.IdResidencia = residencias[0].Id;

        this.novedadSeleccionada = novedad;
    }.bind(this));
};

NovedadController.prototype.esModificacion = function esModificacion() {
    return this.novedadSeleccionada && this.novedadSeleccionada.Id > 0;
};

NovedadController.prototype.guardarNovedad = function guardarNovedad() {
    if (this.esModificacion()){
        return this.novedadApi.modificar(this.novedadSeleccionada).then(function(novedadModificada){
            this.novedadSeleccionada.VersionFila = novedadModificada.VersionFila;
            angular.extend(Object.getPrototypeOf(this.novedadSeleccionada), this.novedadSeleccionada);
            this.novedadSeleccionada = undefined;
        }.bind(this));
    }

    this.novedadSeleccionada.Comentarios = this.comentario.Descripcion ? [this.comentario] : undefined;

    this.novedadApi.agregar(this.novedadSeleccionada).then(function(nuevaNovedad){
        this.novedadSeleccionada.Id = nuevaNovedad.Id;
        this.novedadSeleccionada.VersionFila = nuevaNovedad.VersionFila;
        this.novedades.push(this.novedadSeleccionada);
        if (nuevaNovedad.Comentarios && nuevaNovedad.Comentarios.length > 0)
            this.comentarios.push(nuevaNovedad.Comentarios[0])

        this.novedadSeleccionada = undefined;
    }.bind(this));
};

NovedadController.prototype.agregarComentario = function agregarComentario() {
    var comentario = angular.copy(this.comentario);
    comentario.IdNovedad = this.novedadSeleccionada.Id;

    this.comentarioApi.agregar(comentario).then(function(comentario) {
        this.comentario.Descripcion = undefined;
        this.comentarios.push(comentario);
    }.bind(this));
};

NovedadController.prototype.puedeAgregarNovedad = function puedeAgregarNovedad() {
    return this.sesionService.sesion.EsAdministrador || this.sesionService.sesion.EsResidente;
}

NovedadController.prototype.puedeCambiarAsignacionNovedad = function puedeCambiarAsignacionNovedad() {
    return this.sesionService.sesion.EsAdministrador;
}

NovedadController.prototype.puedeCambiarEstadoNovedad = function puedeCambiarEstadoNovedad() {
    return this.sesionService.sesion.EsAdministrador || this.sesionService.sesion.EsEmpleado;
}

NovedadController.prototype.puedeCambiarNovedad = function puedeCambiarNovedad() {
    return this.sesionService.sesion.EsAdministrador || this.sesionService.sesion.EsResidente;
}

