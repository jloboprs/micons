var ConjuntoController = function ConjuntoController(conjuntoApi, residenciaApi, conjuntosValue, NgTableParams) {
    this.esResidencia = false;
    this.conjuntoSeleccionado = undefined;
    this.residenciaApi = residenciaApi;
    this.conjunto = new TablaController(conjuntoApi, conjuntosValue, NgTableParams);
    this.residencia = new TablaController(residenciaApi, [], NgTableParams);
    this.residencia.onGuardar = this.onGuardarResidencia.bind(this);
};

ConjuntoController.prototype.seleccionarConjunto = function seleccionarConjunto(conjunto) {
    this.conjuntoSeleccionado = conjunto;
    var consulta = {
        Filters: [{
            Field: 'IdConjunto',
            Value: conjunto.Id,
            Operator: 'eq'
        }]
    };
    
    this.residenciaApi.obtener(consulta).then(function(respuesta) {
        this.residencia.asignarDatos(respuesta);
        this.esResidencia = true;
    }.bind(this))
};

ConjuntoController.prototype.onGuardarResidencia= function onGuardarResidencia(registro) { 
    registro.IdConjunto = this.conjuntoSeleccionado.Id;
};