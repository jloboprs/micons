var NgDocumentoUnicoValidation = function NgDocumentoUnicoValidation(personaApi) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function link(scope, element, attrs, controllers) {
            var validacion = Object.create(NgDocumentoUnicoValidation.prototype);
            
            validacion.personaApi = personaApi;
            validacion.controllers = controllers;
            validacion.validado = false;
            validacion.persona = undefined;
            validacion.onValidando = scope.$eval(attrs.ngValidando);
            validacion.asignarNoValidado = validacion.asignarNoValidado.bind(validacion);
            validacion.__asignarValidacionPersona = validacion.__asignarValidacionPersona.bind(validacion)
            validacion.validarPersona = validacion.validarPersona.bind(validacion);

            scope.$watch(attrs.ngDocumentoUnico, validacion.asignarPersona.bind(validacion));
            scope.$watch(attrs.ngDocumentoUnico+'.NumeroDocumento', validacion.asignarNoValidado);
            scope.$watch(attrs.ngDocumentoUnico+'.IdTipoDocumento', validacion.asignarNoValidado);
            scope.$watch(attrs.ngDocumentoUnico+'.IdTipoDocumento', validacion.validarPersona);
            element.on('blur', validacion.validarPersona);
        }
    };
}

NgDocumentoUnicoValidation.prototype.asignarPersona = function asignarPersona(persona) {
    this.persona = persona;
};

NgDocumentoUnicoValidation.prototype.asignarNoValidado = function asignarNoValidado() {
    this.validado = false;
};

NgDocumentoUnicoValidation.prototype.validarPersona = function validarPersona() {
    if (!this.validado && this.persona && this.persona.NumeroDocumento && this.persona.IdTipoDocumento)
        this.personaApi.obtenerPorDocumento(this.persona).then(this.__asignarValidacionPersona);
};

NgDocumentoUnicoValidation.prototype.__asignarValidacionPersona = function __asignarValidacionPersona(persona) {
    this.validado = true;
    var validacionCliente = this.onValidando ? this.onValidando(persona, this.persona) : undefined;
    var validacion = validacionCliente === undefined ? persona === undefined || persona.Id === this.persona.Id : validacionCliente;

    this.controllers.$setValidity('documentoUnico', validacion);
};