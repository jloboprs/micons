var NgEmailUnicoValidation = function NgEmailUnicoValidation(usuarioApi) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function link(scope, element, attrs, controllers) {
            var validacion = Object.create(NgEmailUnicoValidation.prototype);
            
            validacion.usuarioApi = usuarioApi;
            validacion.controllers = controllers;
            validacion.validado = false;
            validacion.usuario = undefined;
            validacion.onValidando = scope.$eval(attrs.ngValidando);

            scope.$watch(attrs.ngEmailUnico, validacion.asignarUsuario.bind(validacion));
            element.on('change', validacion.asignarNoValidado.bind(validacion));
            element.on('blur', validacion.validarEmail.bind(validacion));
        }
    };
}

NgEmailUnicoValidation.prototype.asignarUsuario = function asignarUsuario(usuario) {
    this.usuario = usuario;
};

NgEmailUnicoValidation.prototype.asignarNoValidado = function asignarNoValidado() {
    this.validado = false;
};

NgEmailUnicoValidation.prototype.validarEmail = function validarEmail() {
    if (!this.validado && this.usuario && this.usuario.Email)
        this.usuarioApi.obtenerPorEmail(this.usuario.Email).then(this.__asignarValidacionEmail.bind(this));
};

NgEmailUnicoValidation.prototype.__asignarValidacionEmail = function __asignarValidacionEmail(usuario) {
    this.validado = true;
    var validacionCliente = this.onValidando ? this.onValidando(usuario, this.usuario) : undefined;
    var validacion = validacionCliente === undefined ? usuario === undefined || usuario.Id === this.usuario.Id : validacionCliente;

    this.controllers.$setValidity('emailUnico', validacion);
};