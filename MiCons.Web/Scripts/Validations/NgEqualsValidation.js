var NgEqualsValidation = function NgEqualsValidation() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function link(scope, element, attrs, controllers) {
            var validacion = Object.create(NgEqualsValidation.prototype);
            
            validacion.modelo = undefined;
            validacion.comparacion = undefined;
            validacion.controllers = controllers;

            scope.$watch(attrs.ngModel, validacion.asignarModelo.bind(validacion));
            scope.$watch(attrs.ngEquals, validacion.asignarComparacion.bind(validacion));
        }
    };
}

NgEqualsValidation.prototype.asignarModelo = function asignarModelo(modelo) {
    this.modelo = modelo;
    this.validar();
};

NgEqualsValidation.prototype.asignarComparacion = function asignarComparacion(comparacion) {
    this.comparacion = comparacion;
    this.validar();
};

NgEqualsValidation.prototype.validar = function validar() {
    this.controllers.$setValidity('equals', this.modelo == this.comparacion);
};