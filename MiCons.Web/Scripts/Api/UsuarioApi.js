var UsuarioApi = function UsuarioApi($http) {
    ApiBase.call(this, $http);
};

UsuarioApi.prototype = Object.create(ApiBase.prototype);
UsuarioApi.prototype.constructor = UsuarioApi;

UsuarioApi.prototype.obtenerPorEmail = function obtenerPorEmail(email) {
    return this.__hacerSolicitud('GET', 'obtenerPorEmail', { params: { email: email } })
        .then(this.__retornarRespuestaData);
};

ApiBase.prototype.obtenerEmpleados = function obtenerEmpleados(solicitud) {
    return this.__hacerSolicitud('GET','obtenerEmpleados', { params: { solicitud: solicitud } }, 'application/json')
    	.then(this.__retornarRespuestaData);
};

UsuarioApi.prototype.autenticar = function autenticar(usuario, persistente) {
    var dato = { params: { usuario: usuario,  persistente: persistente } };

    return this.__hacerSolicitud('HEAD', 'Autenticar', dato, 'application/json')
        .then(this.__retornarRespuestaExistente);
};

UsuarioApi.prototype.cambiarClave = function cambiarClave(usuario) {
    var dato = { data: { usuario: usuario } };

    return this.__hacerSolicitud('PUT', 'cambiarClave', dato, 'application/json')
        .then(this.__retornarRespuestaData);
};