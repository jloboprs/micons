var ResidenteApi = function ResidenteApi($http) {
    ApiBase.call(this, $http);
};

ResidenteApi.prototype = Object.create(ApiBase.prototype);
ResidenteApi.prototype.constructor = ResidenteApi;

ResidenteApi.prototype.agregar = function agregar(residente) {
    return ApiBase.prototype.agregar.call(this, this.__limpiarResidente(residente))
};

ResidenteApi.prototype.modificar = function modificar(residente) {
    return ApiBase.prototype.modificar.call(this, this.__limpiarResidente(residente))
};

ResidenteApi.prototype.__limpiarResidente = function __limpiarResidente(residente) {
    residente = angular.extend({}, residente);
    residente.Persona = undefined;
    residente.Residencia = undefined;
    
    return residente;
};