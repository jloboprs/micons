var ApiBase = function ApiBase($http) {
    this.$http = $http;
};

ApiBase.prototype.obtener = function obtener(solicitud) {
    return this.__hacerSolicitud('GET','obtener', { params: { solicitud: solicitud } }, 'application/json').then(this.__retornarRespuestaData);
};

ApiBase.prototype.obtenerPorId = function obtenerPorId(id) {
    return this.__hacerSolicitud('GET','index', { params: { id: id } }).then(this.__retornarRespuestaData);
};

ApiBase.prototype.agregar = function agregar(objeto) {
    return this.__hacerSolicitud('POST','index', { data: objeto }, 'application/json').then(this.__retornarRespuestaData);
};

ApiBase.prototype.modificar = function modificar(objeto) {
    return this.__hacerSolicitud('PUT','index', { data: objeto }, 'application/json').then(this.__retornarRespuestaData);
};

ApiBase.prototype.remover = function remover(objeto) {
    return this.__hacerSolicitud('DELETE','remover', { data: objeto }, 'application/json').then(this.__retornarRespuestaData);
};

ApiBase.prototype.removerPorId = function removerPorId(id) {
    return this.__hacerSolicitud('DELETE','index', { params: { id: id } }).then(this.__retornarRespuestaData);
};

ApiBase.prototype.__hacerSolicitud = function __hacerSolicitud(metodo, accion, value, type) {
    var configuracion = { 
        data: '',
        method: metodo.toUpperCase(),
        url: this.__obtenerAccionUrl(accion)
    };

    if (value && value.params)
        configuracion.params = value.params;
    
    if  (value && value.data)
        configuracion.data = value.data;

    if (type)
        configuracion.headers = { 'Content-Type': type };

    return this.$http(configuracion);
};

ApiBase.prototype.__obtenerAccionUrl = function __obtenerAccionUrl(accion) {
    var nombreFuncion = this.constructor.name;
    
    var controlador = nombreFuncion.slice(-3).toLowerCase() === 'api'
        ? nombreFuncion.slice(0,-3)
        : nombreFuncion;

    return ['/api/', controlador, '/', accion].join('');
};

ApiBase.prototype.__retornarRespuestaData = function __retornarRespuestaData(respuesta) { 
    return respuesta.data; 
};

ApiBase.prototype.__retornarRespuestaExistente = function __retornarRespuestaExistente(respuesta) { 
    return respuesta.status === 200 || respuesta.status === 204; 
};