var PersonaApi = function PersonaApi($http) {
    ApiBase.call(this, $http);
};

PersonaApi.prototype = Object.create(ApiBase.prototype);
PersonaApi.prototype.constructor = PersonaApi;

PersonaApi.prototype.obtenerPorDocumento = function obtenerPorDocumento(persona) {
    return this.__hacerSolicitud('GET','obtenerPorDocumento', { params: persona })
        .then(this.__retornarRespuestaData);
};

PersonaApi.prototype.activar = function activar(persona) {
    return this.__hacerSolicitud('PUT','activar', { data: persona }, 'application/json')
        .then(this.__retornarRespuestaData);
};