var ComentarioApi = function ComentarioApi($http) {
    ApiBase.call(this, $http);
};

ComentarioApi.prototype = Object.create(ApiBase.prototype);
ComentarioApi.prototype.constructor = ComentarioApi;

ComentarioApi.prototype.obtenerPorNovedad = function obtenerPorDocumento(idNovedad) {
    return this.__hacerSolicitud('GET','obtenerPorNovedad', { params: { idNovedad : idNovedad } })
        .then(this.__retornarRespuestaData);
};