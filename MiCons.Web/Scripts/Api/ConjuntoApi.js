var ConjuntoApi = function ConjuntoApi($http) {
    ApiBase.call(this, $http);
};

ConjuntoApi.prototype = Object.create(ApiBase.prototype);
ConjuntoApi.prototype.constructor = ConjuntoApi;

ConjuntoApi.prototype.obtenerPorPersona = function obtenerPorPersona(idPersona) {
    return this.__hacerSolicitud('GET','ObtenerPorPersona', { params: { idPersona: idPersona } }, 'application/json').then(this.__retornarRespuestaData);
};