var DefaultInterceptor = function DefaultInterceptor($q) {
    this.$q = $q;
    this.responseError = this.responseError.bind(this);
    this.alert = alert.danger || alert;

    this.NotModified = 304;
    this.BadRequest = 400;
    this.Unauthorized = 401;
    this.Forbidden = 403;
    this.NotFound = 404;
    this.InternalServerError = 500;
}

DefaultInterceptor.prototype.responseError = function responseError(response) {
    var mensaje = undefined;
    
    if (response.status === this.NotModified)
        mensaje = 'No se pudo actualizar el registro porque está desactualizado, por favor refresque la página';

    else if (response.status === this.BadRequest)
        mensaje = 'La solicitud realizada es incorrecta, Por favor intente de nuevo';

    else if (response.status === this.Unauthorized)
        mensaje = 'Debe iniciar sesión para realizar la solicitud';

    else if (response.status === this.Forbidden)
        mensaje = 'No tiene permiso para realizar la solicitud';

    else if (response.status === this.InternalServerError)
        mensaje = 'Oops lo sentimos, Se ha presentado un error interno en la aplicación';

    else if (response.status !== this.NotFound)
        mensaje = 'Oops lo sentimos, Se ha presentado un error desconocido en la aplicación';

    if (mensaje !== undefined) {
        this.alert(mensaje);
        return this.$q.reject(response);
    }

    response.data = undefined;
    return this.$q.resolve(response);
};