var SesionService = function SesionService($cookieStore) {
    this.$cookieStore = $cookieStore;
    this.sesion = undefined;
    
    this.asignarSesion();
};

SesionService.prototype.asignarSesion = function asignarSesion() {
    this.sesion = this.$cookieStore.get('.JSXAUTH');
};

SesionService.prototype.estaAutenticado = function estaAutenticado() {
    return this.sesion ? true : false;
};