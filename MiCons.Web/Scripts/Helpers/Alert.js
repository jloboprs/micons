var alert = function alert(message, type) {
    return Object.create(alert.prototype).init(message, type);
};

alert.prototype.init = function(message, type) {
    this.index = 0;
    this.elemnt = null;
    this.closed = false;
    this.message = message;
    this.type = type || 'info';
    this.close = this.close.bind(this);
    this.onClosed = this.onClosed.bind(this);

    this.generate();
    return this;
};

alert.prototype.generate = function generate() {
    this.index = this.getLength();
    this.setLength(this.index + 1);
    
    this.elemnt = this.getTemplate().alert()
        .on('closed.bs.alert', this.onClosed)
        .appendTo($(document.body));

    this.elemnt.css('top', this.index * this.elemnt.outerHeight(true));
    
    setTimeout(this.close, 8000);
};

alert.prototype.getTemplate = function getTemplate() {
    return $(['<div class="alert alert-position alert-', this.type, ' alert-inner-message">',
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
            '<strong>', this.type.toUpperCase(), ':</strong> ', this.message,
        '</div>'].join(''));
};

alert.prototype.close = function close() {
    if (!this.closed)
        this.elemnt.alert('close');
};

alert.prototype.onClosed = function onClosed() {
    this.closed = true;
    if (this.getLength() > this.index)
        this.setLength(this.index);
};

alert.prototype.getLength = function getLength() {
    return alert.prototype.length || 0;
};

alert.prototype.setLength = function setLength(value) {
    alert.prototype.length = value;
};

alert.success = function success(message) {
    return alert(message, 'success');
};

alert.danger = function danger(message) {
    return alert(message, 'danger');
};

alert.info = function info(message) {
    return alert(message, 'info');
};

alert.warning = function warning(message) {
    return alert(message, 'warning');
};