var XtFormConfig = function XtFormConfig(xtFormConfigProvider) {
    xtFormConfigProvider.setErrorMessages({
        minlength: 'Tiene que ser al menos {{ngMinlength}} caracteres',
        maxlength: 'Puede no más de {{ngMaxlength}} caracteres',
        required: 'Este campo es obligatorio',
        number: 'Debe ser un número',
        min: 'Debe ser al menos {{ngMin}}',
        max: 'Debe ser mayor que {{ngMax}}',
        email: 'Debe ser una dirección de correo electrónica válida',
        pattern: 'Valor ilegal',
        url: 'Debe ser una URL válida',
        date: 'Debe ser una fecha válida',
        datetimelocal: 'Debe ser una fecha válida',
        time: 'Debe ser una hora válida',
        week: 'Debe ser una semana válida',
        month: 'Debe ser un mes válido',
        emailUnico: 'El email ya se encuentra en uso',
        documentoUnico: 'El documento ya se encuentra en uso',
        equals: 'Los campos no son iguales'
    });
};