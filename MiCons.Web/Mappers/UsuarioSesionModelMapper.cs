﻿using MiCons.Data.Entities;
using MiCons.Web.Models;

namespace MiCons.Web.Mappers
{
    public class UsuarioSesionModelMapper : IUsuarioSesionModelMapper
    {
        public UsuarioSesionModel Mapear(Persona persona, string email, bool esResidente, bool esEmpleado, bool esAdministrador)
        {
            return new UsuarioSesionModel
            {
                Email = email,
                EsResidente = esResidente,
                EsEmpleado = esEmpleado,
                EsAdministrador = esAdministrador,
                IdPersona = persona.Id,
                Nombre = persona.Nombre,
                Activo = persona.Activo,
                IdTipoDocumento = persona.IdTipoDocumento,
                NumeroDocumento = persona.NumeroDocumento
            };
        }
    }
}