﻿using MiCons.Data.Entities;
using MiCons.Web.Models;

namespace MiCons.Web.Mappers
{
    public interface IUsuarioSesionModelMapper
    {
        UsuarioSesionModel Mapear(Persona persona, string email, bool esResidente, bool esEmpleado, bool esAdministrador);
    }
}