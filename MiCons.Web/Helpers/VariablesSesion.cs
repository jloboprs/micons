﻿using MiCons.Data.Repositories;
using MiCons.Web.Mappers;
using MiCons.Web.Models;

namespace MiCons.Web.Helpers
{
    public class VariablesSesion : IVariablesSesion
    {
        IPersonaRepo _personaRepo;
        IUsuarioSesionModelMapper _usuarioMapper;

        public VariablesSesion(IPersonaRepo personaRepo, IUsuarioSesionModelMapper usuarioMapper)
        {
            _personaRepo = personaRepo;
            _usuarioMapper = usuarioMapper;
        }

        public UsuarioSesionModel Usuario
        {
            get
            {
                var usuario = ContextValue.Session["usuario"] as UsuarioSesionModel;
                if (usuario != null)
                    return usuario;

                if (!ContextValue.EstaAutenticado)
                    return null;

                var persona = _personaRepo.ObtenerPorEmail(ContextValue.UserName);
                var esResidente = _personaRepo.EsResidente(persona.Id);
                var esAdministrador = _personaRepo.EsAdministrador(persona.Id);
                var esEmpleado = esAdministrador || _personaRepo.EsEmpleado(persona.Id);

                ContextValue.Session["usuario"] = usuario = _usuarioMapper.Mapear(persona, ContextValue.UserName, esResidente, esEmpleado, esAdministrador);

                return usuario;
            }
        }
    }
}