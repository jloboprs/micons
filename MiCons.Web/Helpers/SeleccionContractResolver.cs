﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MiCons.Web.Helpers
{
    public class SeleccionContractResolver : DefaultContractResolver
    {
        IDictionary<Type, ICollection<string>> _propiedades;

        public SeleccionContractResolver()
        {
            _propiedades = new Dictionary<Type, ICollection<string>>();
        }

        public SelectorPropiedades<T> Quitar<T>()
        {
            return Quitar<T>(_propiedades);
        }

        private SelectorPropiedades<T> Quitar<T>(IDictionary<Type, ICollection<string>> propiedades)
        {
            var tipo = typeof(T);
            if (!propiedades.ContainsKey(tipo))
                propiedades[tipo] = new List<string>();

            return new SelectorPropiedades<T>(propiedades[tipo]);
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            if (!_propiedades.Any())
                return property;

            var tipo = property.DeclaringType;
            if (_propiedades.ContainsKey(tipo) && _propiedades[tipo].Contains(property.PropertyName))
            {
                property.ShouldSerialize = instance => false;
                return property;
            }

            var tipoHerencia = _propiedades.Keys.FirstOrDefault(t => tipo.IsSubclassOf(t));
            if (tipoHerencia != null && _propiedades[tipoHerencia].Contains(property.PropertyName))
            {
                property.ShouldSerialize = instance => false;
                return property;
            }

            return property;
        }
    }
}