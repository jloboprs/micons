﻿using System;
using MiCons.Data.Entities;

namespace MiCons.Web.Helpers
{
    public interface IGeneradorHash : IDisposable
    {
        bool Comparar(string texto, string hash);
        bool Comparar(Usuario usuario, Persona persona, string hash);
        string GenerarHash(string texto, byte[] sald = null);
        string GenerarHash(Usuario usuario, Persona persona);
    }
}