﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.UI;

namespace MiCons.Web.Helpers
{
    public static class ContextValue
    {
        private static HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        private static HttpResponse Response
        {
            get { return HttpContext.Current.Response; }
        }

        public static HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        public static IPrincipal User
        {
            get { return HttpContext.Current.User; }
            set { HttpContext.Current.User = value; }
        }

        public static string UserName
        {
            get { return User != null ? User.Identity.Name : string.Empty; }
        }

        public static Page Page
        {
            get { return HttpContext.Current.Handler as Page; }
        }

        public static string VirtualPath
        {
            get { return Page.AppRelativeVirtualPath; }
        }

        static UrlHelper _url;
        public static UrlHelper Url
        {
            get
            {
                if (_url == null || _url.RequestContext != Request.RequestContext)
                    _url = new UrlHelper(Request.RequestContext, RouteTable.Routes);

                return _url;
            }
        }

        public static string NombreAplicacion
        {
            get { return Request.Url.Host.ToUpperFirstChar(); }
        }

        public static Uri UrlUri
        {
            get { return Request.Url; }
        }

        public static bool EstaAutenticado
        {
            get { return !string.IsNullOrEmpty(UserName); }
        }

        public static HttpCookie ObtenerCookie(string index)
        {
            return Request.Cookies[index];
        }

        public static void AsignarCookie(HttpCookie cookie)
        {
            Response.Cookies.Add(cookie);
        }
    }
}