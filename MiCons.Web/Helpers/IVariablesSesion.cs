﻿using MiCons.Web.Models;

namespace MiCons.Web.Helpers
{
    public interface IVariablesSesion
    {
        UsuarioSesionModel Usuario { get; }
    }
}