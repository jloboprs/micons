﻿using MiCons.Web.Dependencies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace MiCons.Web.Helpers
{
    public static class ExtensionHelper
    {

        public static MvcHtmlString ObtenerJSON<TEnum>(this HtmlHelper helper)
        {
            var enumeracion = new Dictionary<string, int>();

            foreach (var valor in Enum.GetValues(typeof(TEnum)))
            {
                enumeracion.Add(valor.ToString(), (int) valor);
            }

            return MvcHtmlString.Create(JsonConvert.SerializeObject(enumeracion));
        }

        public static string ToUpperFirstChar(this string texto)
        {
            if (String.IsNullOrEmpty(texto))
                return texto;

            return texto.First().ToString().ToUpper() + texto.Substring(1);
        }

        public static string Content(this UrlHelper urlHelper, string contentPath, bool absoluto)
        {
            var path = urlHelper.Content(contentPath);
            if (!absoluto)
                return path;

            return new Uri(ContextValue.UrlUri, path).AbsoluteUri;
        }

        public static Bundle Include<T>(this Bundle bundle) where T : DependenciaBase, new()
        {
            return bundle.Include(SolicionadorDependencia.ObtenerDependenciasBundle(new T()));
        }
    }
}