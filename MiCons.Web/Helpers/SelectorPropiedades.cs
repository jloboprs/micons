﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MiCons.Web.Helpers
{
    public class SelectorPropiedades<T>
    {
        public ICollection<string> Propiedades { get; private set; }

        public SelectorPropiedades(ICollection<string> propiedades)
        {
            Propiedades = propiedades;
        }

        public SelectorPropiedades<T> Propiedad<Y>(Expression<Func<T, Y>> expresion)
        {
            var propiedad = expresion.Body as MemberExpression;

            if (propiedad == null)
                throw new InvalidOperationException();

            Propiedades.Add(propiedad.Member.Name);

            return this;
        }
    }
}