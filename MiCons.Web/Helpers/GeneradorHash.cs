﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using MiCons.Data.Entities;

namespace MiCons.Web.Helpers
{
    public class GeneradorHash : IGeneradorHash
    {

        const int _minTamanoSalt = 4;
        const int _maxTamanoSalt  = 8;

        HashAlgorithm _algoritmo;
        RandomNumberGenerator _random;

        private int TamanoHashBytes { get { return _algoritmo.HashSize / 8; } }

        public GeneradorHash(HashAlgorithm algoritmo, RandomNumberGenerator random)
        {
            _algoritmo = algoritmo;
            _random = random;
        }

        public string GenerarHash(Usuario usuario, Persona persona)
        {
            return GenerarHash(ObtenerCodigo(usuario, persona));
        }

        public string GenerarHash(string texto, byte[] sald = null)
        {
            sald = sald ?? GenerarSalt();

            var textoConSald = Encoding.UTF8.GetBytes(texto).Concat(sald).ToArray();

            var hashConSalt = _algoritmo.ComputeHash(textoConSald).Concat(sald).ToArray();

            return Convert.ToBase64String(hashConSalt);
        }

        public bool Comparar(Usuario usuario, Persona persona, string hash)
        {
            return Comparar(ObtenerCodigo(usuario, persona), hash);
        }

        public bool Comparar(string texto, string hash)
        {
            byte[] hashBytes = Convert.FromBase64String(hash);

            if (hashBytes.Length < TamanoHashBytes)
                return false;

            var salt = hashBytes.Skip(TamanoHashBytes).Take(hashBytes.Length - TamanoHashBytes).ToArray();

            return (hash == GenerarHash(texto, salt));
        }

        public void Dispose()
        {
            _algoritmo.Dispose();
        }

        private byte[] GenerarSalt()
        {
            var random = new Random();
            var saltBytes = new byte[random.Next(_minTamanoSalt, _maxTamanoSalt)];

            _random.GetNonZeroBytes(saltBytes);

            return saltBytes;
        }

        private string ObtenerCodigo(Usuario usuario, Persona persona)
        {
            return string.Format("{0}|{1}|{2}|{3}", usuario.Email, usuario.Clave, persona.IdTipoDocumento, persona.NumeroDocumento);
        }
    }
}
