﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Web;
using System.Web.Mvc;

namespace MiCons.Web.Helpers
{
    public class JsonNetResult : JsonResult
    {
        public JsonNetResult(object data)
        {
            ContentType = "application/json";
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            ExecuteResult(context.HttpContext.Response);
        }

        public void ExecuteResult(HttpResponse response)
        {
            ExecuteResult(new HttpResponseWrapper(response));
        }

        public void ExecuteResult(HttpResponseBase response)
        {
            response.ContentType = ContentType;
            response.ContentEncoding = ContentEncoding ?? response.ContentEncoding;
            response.Write(JsonConvert.SerializeObject(Data));
        }
    }
}