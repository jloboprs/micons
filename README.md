# MiCons
Aplicación para administración y resolución de novedades para conjuntos o edificios residenciales.

## [Manual de usuario](https://www.youtube.com/watch?v=-anh0YXQU3s&list=PL8gKCBdi2r64_nWX9Y8sTvEHR4oWxzQfI)

## Cuenta
* admin@micons.com
* 123456

## Diccionario
* Novedad: Es la solicitud del residente para la realización de un trabajo por parte del administrador o trabajador del conjunto. El cual maneja diferentes estados a los largo del tiempo.
* Estados: Son los estados que tienen las novedades desde su creación a su cierre. Abierto, en curso, cerrado.
* Perfiles: Son diferentes tipos de personas que interactúan en la aplicación. Administrador, residente, contable, mantenimiento.
* Residentes: Pueden realizar novedades en nombre de su residencia.
* Administrador: Pueden ver todas las novedades del conjunto al que es asignado.
* Otros perfiles: Son perfiles de asistencia para la resolución de novedades. Estos pueden ver y resolver las novedades que le son asignadas.

## Requerimientos
### Resolución de novedades del Administrador
* El administrador ve todas las novedades del conjunto que tiene asignado
* Selecciona una novedad y la marca como en curso
* Escribe un comentario y da por cerrada la novedad

### Reasignación del administrador a persona
* El administrador ve todas las novedades del conjunto que tiene asignado
* Selecciona una novedad
* Asigna la novedad a una persona
* El sistema crea un comentario de reasignación

### Reasignación del administrador al perfil
* El administrador ve todas las novedades del conjunto que tiene asignado
* Selecciona una novedad
* Asigna la novedad a un perfil como el contable
* El sistema crea un comentario de reasignación

### Resolución de novedades de perfiles
* El usuario ve las novedades asignadas a él o al perfil.
* Selecciona una novedad y la marca como en curso
* Asigna la novedad a un perfil como el contable
* Escribe un comentario y da por cerrada la novedad

### Reasignación por perfil
* El usuario ve las novedades asignadas a él o al perfil.
* Selecciona una novedad
* Asigna la novedad a un perfil como el contable o a un usuario
* El sistema crea un comentario de reasignación

### Creación de novedad por Usuario
* El usuario ve el historial de las novedades de su residencia
* El usuario selecciona crear una novedad
* El usuario agrega el asunto de la novedad y le coloca una breve descripción
 
### Creación de novedad por Usuario con asignación
* El usuario ve el historial de las novedades de su residencia
* El usuario selecciona crear una novedad
* El usuario asigna la novedad a una persona o un perfil como el de mantenimiento
* El usuario agrega el asunto de la novedad y le coloca una breve descripción

## Diagrama de clases
<img src="https://drive.google.com/uc?export=view&id=0BxOxlyTdfeOvWWNxS2UxZ01WbTQ">